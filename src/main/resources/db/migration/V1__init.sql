-- ROLES
create table roles
(
    type varchar(255) not null
        constraint roles_pkey
            primary key
);

alter table roles
    owner to "${user}";

-- EMPLOYEES
create table employees
(
    id bigserial not null
        constraint employees_pkey
        primary key,
    email varchar(255)
        constraint uk_j9xgmd0ya5jmus09o0b8pqrpb
        unique,
    name  varchar(255)
);

alter table employees
    owner to "${user}";

-- TABLES
create table employee_tables
(
    id           bigserial not null
        constraint employee_tables_pkey
            primary key,
    floor        integer,
    internal_id  integer,
    table_number integer,
    employee_id  bigint
        constraint fks4i430jy04en7uuv3bkjjmae4
            references employees
);

alter table employee_tables
    owner to "${user}";


-- EMPLOYEES_ROLES
create table employees_roles
(
    employee_id bigint       not null
        constraint fkr9b8ry8qtdtoc8pcw56ug54x5
            references employees,
    role_type   varchar(255) not null
        constraint fk14otavk6lq6m5eck5gjwvdjqu
            references roles,
    constraint employees_roles_pkey
        primary key (employee_id, role_type)
);

alter table employees_roles
    owner to "${user}";

