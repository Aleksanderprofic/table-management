alter table employees
    add column account varchar,
    add column employee_table_id int8;

alter table employee_tables
    add column x float,
    add column y float;