create table floors
(
    floor_number integer not null
        constraint floors_pkey
            primary key,
    image_height integer default 1264,
    image_width  integer default 1550
);

alter table floors
    owner to "${user}";

insert into floors (floor_number, image_width, image_height) values (1, 1550, 1264);
insert into floors (floor_number, image_width, image_height) values (3, 1550, 1264);

create table walls
(
    id                      bigserial not null
        constraint walls_pkey
            primary key,
    border_color            integer default 0,
    x_percentage_end        double precision check (0.0 <= x_percentage_end AND x_percentage_end <= 100.0),
    x_percentage_start      double precision check (0.0 <= x_percentage_start AND x_percentage_start <= 100.0),
    y_percentage_end        double precision check (0.0 <= y_percentage_end AND y_percentage_end <= 100.0),
    y_percentage_start      double precision check (0.0 <= y_percentage_start AND y_percentage_start <= 100.0),
    filling_color           integer default 16777215,
    joined_with_other_walls boolean default true,
    thickness_percentage    double precision default 0.00322,
    floor_id                integer
        constraint fkhgfquwidt1cg7x78nc5ggr8o2
            references floors
);

alter table walls
    owner to "${user}";


