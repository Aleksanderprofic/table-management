CREATE SEQUENCE room_descriptions_id_seq start 99 increment by 42;
create table room_descriptions
(
    grid_office_id bigint not null,
    x              double precision default 0,
    y              double precision default 0,
    r              double precision default 0,
    type           bigint           default 1,
    name           varchar(255)     default ''  ::character varying,
    description    varchar(255)     default ''  ::character varying,
    floor_id       bigint not null
    constraint fk_floor
        references floors(id),
    CONSTRAINT rooms_descriptions_pkey
        PRIMARY KEY (grid_office_id, floor_id)
);

alter table room_descriptions
    owner to "${user}";