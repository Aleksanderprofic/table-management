ALTER TABLE employee_tables
  RENAME TO desks;

ALTER TABLE desks
  DROP COLUMN table_number;