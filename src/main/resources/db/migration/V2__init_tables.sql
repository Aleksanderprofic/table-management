insert into roles values ('REGULAR');
insert into roles values ('ADMIN');

insert into employees values (1, 'aprofic@griddynamics.com', 'aprofic');
insert into employees values (2, 'krzonca@griddynamics.com', 'krzonca');

insert into employees_roles values (1, 'ADMIN');
insert into employees_roles values (2, 'REGULAR');