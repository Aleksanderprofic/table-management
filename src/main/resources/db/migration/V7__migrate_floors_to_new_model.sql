delete from walls;
delete from floors;
delete from desks;

create table corners
(
    id bigserial not null
        constraint corners_pkey
            primary key,
    frontend_id varchar(255),
    x double precision,
    y double precision

);

alter table corners
    owner to "${user}";

alter table floors
    rename column floor_number to id;

CREATE SEQUENCE floors_id_seq start 1 increment by 42;

alter table floors
    alter column id set data type bigint,
    alter column id SET DEFAULT nextval('floors_id_seq'),
    drop column image_height,
    drop column image_width,
    add column name varchar(255),
    add column is_loaded_from_spreadsheet bool default false,
    add column spreadsheet_floor varchar(255);

ALTER SEQUENCE floors_id_seq OWNED BY floors.id;

CREATE UNIQUE INDEX idx_floors_name
    ON floors(name);

alter table walls
  drop column border_color,
  drop column x_percentage_end,
  drop column x_percentage_start,
  drop column y_percentage_end,
  drop column y_percentage_start,
  drop column filling_color,
  drop column joined_with_other_walls,
  drop column thickness_percentage,

  alter column floor_id set data type bigint,
  add column corner1 bigint,
  add column corner2 bigint,
  ADD CONSTRAINT fk_corner1
  FOREIGN KEY (corner1)
  REFERENCES corners(id),
  ADD CONSTRAINT fk_corner2
  FOREIGN KEY (corner2)
  REFERENCES corners(id);

alter sequence employee_tables_id_seq rename to desks_id_seq;
alter sequence desks_id_seq increment by 42;

alter table desks
    rename column internal_id to grid_office_id;

alter table desks
  alter column grid_office_id set data type bigint,
  alter column grid_office_id set not null,
  drop column id,
  drop column floor,
  add column r float default 0,
  alter column x set default 0,
  alter column y set default 0,
  add column type bigint default 0,
  add column description varchar(255) default '',
  add column floor_id bigint not null,
  ADD CONSTRAINT fk_floor
  FOREIGN KEY (floor_id)
  REFERENCES floors(id),
  ADD CONSTRAINT desks_pkey
    PRIMARY KEY (grid_office_id, floor_id);

alter table employees
   drop column employee_table_id,
   alter column id SET DEFAULT nextval('employees_id_seq');

insert into floors (name, is_loaded_from_spreadsheet, spreadsheet_floor)
values ('First Floor', true, 'FIRST_FLOOR'),
       ('Third Floor', true, 'THIRD_FLOOR');

SELECT setval('employees_id_seq', 20, true);
alter sequence employees_id_seq increment by 42;