package com.gd.tablesmanagement.persistance.repositories;

import com.gd.tablesmanagement.persistance.model.Employee;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

  List<Employee> findByName(String name);

  Optional<Employee> findByEmail(String email);

  List<Employee> findAllByNameIn(List<String> names);
}
