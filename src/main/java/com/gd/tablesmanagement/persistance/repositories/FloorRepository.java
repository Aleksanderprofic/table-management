package com.gd.tablesmanagement.persistance.repositories;

import com.gd.tablesmanagement.persistance.model.Floor;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorRepository extends CrudRepository<Floor, Long> {

  Optional<Floor> findFloorById(Long id);

  boolean existsByName(String name);


}