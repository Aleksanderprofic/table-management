package com.gd.tablesmanagement.persistance.repositories;

import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.Wall;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WallRepository extends CrudRepository<Wall, Long> {

  void deleteAllByFloor(Floor floor);

}
