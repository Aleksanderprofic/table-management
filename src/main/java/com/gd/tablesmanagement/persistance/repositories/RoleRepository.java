package com.gd.tablesmanagement.persistance.repositories;

import com.gd.tablesmanagement.persistance.model.Role;
import com.gd.tablesmanagement.persistance.model.RoleType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

  Role findRoleByRoleType(RoleType roleType);
}
