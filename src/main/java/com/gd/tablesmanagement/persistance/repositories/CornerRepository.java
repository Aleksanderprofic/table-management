package com.gd.tablesmanagement.persistance.repositories;

import com.gd.tablesmanagement.persistance.model.Corner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CornerRepository extends CrudRepository<Corner, Long> {


}
