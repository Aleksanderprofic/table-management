package com.gd.tablesmanagement.persistance.model;

import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "walls")
public class Wall {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "wall_generator")
  @SequenceGenerator(
      name = "wall_generator",
      sequenceName = "walls_id_seq",
      allocationSize = 42)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "floor_id")
  private Floor floor;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "corner1", referencedColumnName = "id")
  private Corner corner1;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "corner2", referencedColumnName = "id")
  private Corner corner2;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Floor getFloor() {
    return floor;
  }

  public void setFloor(Floor floor) {
    this.floor = floor;
  }

  public Corner getCorner1() {
    return corner1;
  }


  /** Sets first corner of the wall and adds the wall to the corner.
   * @param corner1  corner to set.
   */
  public void setCorner1(
      Corner corner1) {

    if (this.corner1 != null) {
      this.corner1.removeFirstCornerWall(this);
    }

    if (corner1 != null) {
      corner1.addFirstCornerWall(this);
    }

    this.corner1 = corner1;
  }

  public Corner getCorner2() {
    return corner2;
  }

  /** Sets second corner of the wall and adds the wall to the corner.
   * @param corner2  corner to set.
   */
  public void setCorner2(Corner corner2) {

    if (this.corner2 != null) {
      this.corner2.removeSecondCornerWall(this);
    }

    if (corner2 != null) {
      corner2.addSecondCornerWall(this);
    }

    this.corner2 = corner2;
  }

  @Override
  public String toString() {
    return "Wall{"
        + "id=" + id
        + ", corner1=" + corner1
        + ", corner2=" + corner2
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Wall)) {
      return false;
    }
    Wall wall = (Wall) o;
    return Objects.equals(getCorner1(), wall.getCorner1())
        && Objects.equals(getCorner2(), wall.getCorner2());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCorner1(), getCorner2());
  }
}
