package com.gd.tablesmanagement.persistance.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FloorItemPK implements Serializable {

  @Column(name = "grid_office_id")
  private Long gridOfficeId;

  @Column(name = "floor_id")
  private Long floorId;

  public FloorItemPK(Long gridOfficeId, Long floorId) {
    this.gridOfficeId = gridOfficeId;
    this.floorId = floorId;
  }

  public FloorItemPK() {
  }

  public Long getGridOfficeId() {
    return gridOfficeId;
  }

  public void setGridOfficeId(Long gridOfficeId) {
    this.gridOfficeId = gridOfficeId;
  }

  public Long getFloorId() {
    return floorId;
  }

  public void setFloorId(Long floorId) {
    this.floorId = floorId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FloorItemPK)) {
      return false;
    }
    FloorItemPK deskPK = (FloorItemPK) o;
    return Objects.equals(getGridOfficeId(), deskPK.getGridOfficeId())
        && Objects.equals(floorId, deskPK.floorId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(getGridOfficeId(), floorId);
  }

  @Override
  public String toString() {
    return "FloorItemPK{"
        + "gridOfficeId=" + gridOfficeId
        + ", floorId=" + floorId
        + '}';
  }
}
