package com.gd.tablesmanagement.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {

  @Id
  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  private RoleType roleType;

  @JsonIgnore
  @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
  private Set<Employee> employees;

  public Role() {
    this.roleType = RoleType.REGULAR;
    this.employees = new HashSet<>();
  }

  public Role(RoleType roleType) {
    this.employees = new HashSet<>();
    this.roleType = roleType;
  }

  public RoleType getRoleType() {
    return roleType;
  }

  public void setRoleType(RoleType roleType) {
    this.roleType = roleType;
  }

  public Set<Employee> getEmployees() {
    return employees;
  }

  public void setEmployees(Set<Employee> employees) {
    this.employees = employees;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Role)) {
      return false;
    }
    return getRoleType() == ((Role) o).getRoleType();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getRoleType());
  }
}