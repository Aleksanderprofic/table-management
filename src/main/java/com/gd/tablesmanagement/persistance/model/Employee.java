package com.gd.tablesmanagement.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "employee_generator")
  @SequenceGenerator(name = "employee_generator",
      sequenceName = "employees_id_seq",
      allocationSize = 42)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "account")
  private String account;

  @Column(name = "email")
  private String email;

  @JsonIgnore
  @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
  private Desk desk = new Desk();

  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
      CascadeType.REFRESH, CascadeType.DETACH})
  @JoinTable(
      name = "employees_roles",
      joinColumns = {@JoinColumn(name = "employee_id")},
      inverseJoinColumns = {@JoinColumn(name = "role_type")})
  private Set<Role> roles;

  public Employee() {
    this.roles = new HashSet<>();
  }

  /**
   * Represents employee entity.
   *
   * @param name  employee's name and surname
   * @param email  employee's email
   * @param desk  employee's desk
   * @param roles  employee's user roles
   */
  public Employee(String name, String email, Desk desk, Set<Role> roles) {
    this.roles = new HashSet<>();
    this.email = email;
    this.name = name;
    this.desk = desk;
    this.roles.addAll(roles);
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  public Desk getDesk() {
    return desk;
  }

  public void setDesk(Desk desk) {
    this.desk = desk;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Employee)) {
      return false;
    }
    Employee employee = (Employee) o;
    return Objects.equals(getName(), employee.getName())
        && Objects.equals(getAccount(), employee.getAccount())
        && Objects.equals(getEmail(), employee.getEmail())
        && Objects.equals(getRoles(), employee.getRoles());
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        getName(),
        getAccount(),
        getEmail(),
        getRoles());
  }

  @Override
  public String toString() {
    return "Employee{"
        + "id=" + id
        + ", name='" + name + '\''
        + ", account='" + account + '\''
        + ", email='" + email + '\''
        + ", roles=" + roles
        + '}';
  }
}
