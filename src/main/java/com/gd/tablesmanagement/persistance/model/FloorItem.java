package com.gd.tablesmanagement.persistance.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.MapsId;

@MappedSuperclass
public class FloorItem {

  @EmbeddedId
  private FloorItemPK idKey = new FloorItemPK();

  @MapsId("floorId")
  @JoinColumn(name = "floor_id", referencedColumnName = "id")
  @ManyToOne
  private Floor floor;

  @Column(name = "x")
  private Double coordinateX = 0D;

  @Column(name = "y")
  private Double coordinateY = 0D;

  @Column(name = "r")
  private Double rotation = 0D;

  @Column(name = "description")
  private String description = "";

  @Column(name = "type")
  private Long type;

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }

  public Double getRotation() {
    return rotation;
  }

  public void setRotation(Double rotation) {
    this.rotation = rotation;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Floor getFloor() {
    return floor;
  }

  public FloorItemPK getIdKey() {
    return idKey;
  }

  public void setIdKey(FloorItemPK key) {
    this.idKey = key;
  }

  public Long getType() {
    return type;
  }

  public void setType(Long type) {
    this.type = type;
  }

  /** Sets floor for given desk and it's id as part of desks' PK.
   * @param floor  floor for the desk
   */
  public void setFloor(Floor floor) {

    if (floor != null) {
      idKey.setFloorId(floor.getId());
    }
    this.floor = floor;
  }


  public Long getGridOfficeId() {
    return getIdKey().getGridOfficeId();
  }

  public void setGridOfficeId(Long gridOfficeId) {
    getIdKey().setGridOfficeId(gridOfficeId);
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FloorItem)) {
      return false;
    }
    FloorItem floorItem = (FloorItem) o;
    return Objects.equals(getCoordinateX(), floorItem.getCoordinateX())
        && Objects.equals(getCoordinateY(), floorItem.getCoordinateY())
        && Objects.equals(getRotation(), floorItem.getRotation())
        && Objects.equals(getDescription(), floorItem.getDescription());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCoordinateX(), getCoordinateY(), getRotation(), getDescription());
  }

  @Override
  public String toString() {
    return "FloorItem{"
        + "coordinateX=" + coordinateX
        + ", coordinateY=" + coordinateY
        + ", rotation=" + rotation
        + ", description='" + description + '\''
        + '}';
  }
}
