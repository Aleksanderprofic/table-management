package com.gd.tablesmanagement.persistance.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "corners")
public class Corner {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "corner_generator")
  @SequenceGenerator(
      name = "corner_generator",
      sequenceName = "corners_id_seq",
      allocationSize = 42)
  private Long id;

  @Column(name = "frontend_id")
  private String frontendId;

  @OneToMany(mappedBy = "corner1")
  private List<Wall> firstCornerWalls =  new LinkedList<>();

  @OneToMany(mappedBy = "corner2")
  private List<Wall> secondCornerWalls =  new LinkedList<>();

  @Column(name = "x")
  private Double coordinateX;

  @Column(name = "y")
  private Double coordinateY;


  /**  Adds to corner relation in which it's first corner of given wall.
   *
   * @param wall  wall the corner is first corner of
   */
  public void addFirstCornerWall(
      Wall wall) {

    if (firstCornerWalls.contains(wall)) {
      return;
    }

    this.firstCornerWalls.add(wall);
  }

  /**  Adds to corner relation in which it's second corner of given wall.
   *
   * @param wall  wall the corner is second corner of
   */
  public void addSecondCornerWall(
      Wall wall) {

    if (secondCornerWalls.contains(wall)) {
      return;
    }

    this.secondCornerWalls.add(wall);
  }

  /**  Removes from corner relation in which it's first corner of given wall.
   *
   * @param wall  wall the corner is first corner of
   */
  public void removeFirstCornerWall(
      Wall wall) {

    this.firstCornerWalls.remove(wall);

  }

  /**  Remove from corner relation in which it's second corner of given wall.
   *
   * @param wall  wall the corner is second corner of
   */
  public void removeSecondCornerWall(
      Wall wall) {

    this.secondCornerWalls.remove(wall);
  }

  public List<Wall> getFirstCornerWalls() {
    return firstCornerWalls;
  }

  public void setFirstCornerWalls(
      List<Wall> firstCornerWalls) {
    this.firstCornerWalls = firstCornerWalls;
  }

  public List<Wall> getSecondCornerWalls() {
    return secondCornerWalls;
  }

  public void setSecondCornerWalls(
      List<Wall> secondCornerWalls) {
    this.secondCornerWalls = secondCornerWalls;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFrontendId() {
    return frontendId;
  }

  public void setFrontendId(String frontendId) {
    this.frontendId = frontendId;
  }

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Corner)) {
      return false;
    }
    Corner corner = (Corner) o;
    return Objects.equals(getFrontendId(), corner.getFrontendId())
        && Objects.equals(getCoordinateX(), corner.getCoordinateX())
        && Objects.equals(getCoordinateY(), corner.getCoordinateY());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getFrontendId(), getCoordinateX(), getCoordinateY());
  }

  @Override
  public String toString() {
    return "Corner{"
        + "id=" + id
        + ", frontendId='" + frontendId + '\''
        + ", coordinateX=" + coordinateX
        + ", coordinateY=" + coordinateY
        + '}';
  }
}