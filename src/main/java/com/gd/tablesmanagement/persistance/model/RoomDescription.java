package com.gd.tablesmanagement.persistance.model;

import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemType;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "room_descriptions")
public class RoomDescription extends FloorItem {

  @Column(name = "name")
  private String name = "";

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Long getType() {
    return ItemType.ROOM_DESCRIPTION.getTypeId();
  }

  public void setType(Long type) {

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof RoomDescription)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    RoomDescription that = (RoomDescription) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), name);
  }

  @Override
  public String toString() {
    return "RoomDescription{"
        + ", name='" + name + '\''
        + '}';
  }
}
