package com.gd.tablesmanagement.persistance.model;

import com.gd.tablesmanagement.services.google.sheets.floors.GdFloor;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "floors")
public class Floor {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "floor_generator")
  @SequenceGenerator(name = "floor_generator",
      sequenceName = "floors_id_seq",
      allocationSize = 42)
  private Long id;

  @Column(name = "name", unique = true)
  private String name;

  @Column(name = "is_loaded_from_spreadsheet")
  private Boolean isLoadedFromSpreadsheet = false;

  @Column(name = "spreadsheet_floor")
  @Enumerated(EnumType.STRING)
  private GdFloor floor;

  @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Wall> walls = new LinkedList<>();

  @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Desk> desks = new LinkedList<>();

  @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<RoomDescription> roomDescriptions = new LinkedList<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getLoadedFromSpreadsheet() {
    return isLoadedFromSpreadsheet;
  }

  public void setLoadedFromSpreadsheet(Boolean loadedFromSpreadsheet) {
    isLoadedFromSpreadsheet = loadedFromSpreadsheet;
  }

  public GdFloor getFloor() {
    return floor;
  }

  public void setFloor(GdFloor floor) {
    this.floor = floor;
  }

  public List<Wall> getWalls() {
    return walls;
  }

  /** Sets walls for floor and the floor for all walls.
   * @param walls  list of walls
   */
  public void setWalls(List<Wall> walls) {

    if (this.walls != null) {
      this.walls.forEach(w -> w.setFloor(null));
    }

    if (this.walls == null) {
      this.walls = new LinkedList<>();
    }

    this.walls.clear();

    if (walls != null) {
      walls.forEach(w -> w.setFloor(this));
      this.walls.addAll(walls);
    }
  }

  public List<Desk> getDesks() {
    return desks;
  }

  /** Sets desks for floor and the floor for all desks.
   * @param desks  list of walls
   */
  public void setDesks(List<Desk> desks) {

    if (this.desks != null) {
      this.desks.forEach(d -> d.setFloor(null));
    }

    if (this.desks == null) {
      this.desks = new LinkedList<>();
    }

    this.desks.clear();

    if (desks != null) {
      desks.forEach(d -> d.setFloor(this));
      this.desks.addAll(desks);
    }
  }

  /** Adds desk for floor and the floor for the desk.
   * @param desk  desk to add
   */
  public void addDesk(Desk desk) {

    if (desk != null) {
      desk.setFloor(this);
      this.desks.add(desk);
    }
  }

  public List<RoomDescription> getRoomDescriptions() {
    return roomDescriptions;
  }

  /** Sets room descriptions for this and correct floor inside of them them.
   *
   * @param roomDescriptions  room description list to set
   */
  public void setRoomDescriptions(
      List<RoomDescription> roomDescriptions) {

    if (this.roomDescriptions != null) {
      this.roomDescriptions.forEach(d -> d.setFloor(null));
    }

    if (this.roomDescriptions == null) {
      this.roomDescriptions = new LinkedList<>();
    }

    this.roomDescriptions.clear();

    if (roomDescriptions != null) {
      roomDescriptions.forEach(d -> d.setFloor(this));
      this.roomDescriptions.addAll(roomDescriptions);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Floor)) {
      return false;
    }
    Floor floor1 = (Floor) o;
    return Objects.equals(getName(), floor1.getName())
        && Objects.equals(isLoadedFromSpreadsheet, floor1.isLoadedFromSpreadsheet)
        && getFloor() == floor1.getFloor()
        && Objects.equals(getWalls(), floor1.getWalls())
        && Objects.equals(getRoomDescriptions(), floor1.getRoomDescriptions())
        && Objects.equals(getDesks(), floor1.getDesks());
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        getName(),
        isLoadedFromSpreadsheet,
        getFloor(),
        getWalls(),
        getRoomDescriptions(),
        getDesks());
  }

  @Override
  public String toString() {
    return "Floor{"
        + "id=" + id
        + ", name='" + name + '\''
        + ", isLoadedFromSpreadsheet=" + isLoadedFromSpreadsheet
        + ", floor=" + floor
        + ", walls=" + walls
        + ", walls=" + roomDescriptions
        + ", desks=" + desks
        + '}';
  }
}
