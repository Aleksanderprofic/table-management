package com.gd.tablesmanagement.persistance.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

public enum RoleType {
  ADMIN, REGULAR;

  private static final String SPRING_ROLE_PREFIX = "ROLE ";

  public SimpleGrantedAuthority getAsSimpleGrantedAuthority() {
    return new SimpleGrantedAuthority(SPRING_ROLE_PREFIX + this.name());
  }
}