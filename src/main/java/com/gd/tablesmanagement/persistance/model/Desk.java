package com.gd.tablesmanagement.persistance.model;

import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemType;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "desks")
public class Desk extends FloorItem {

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "employee_id")
  private Employee employee;

  public Desk() {
  }

  public Employee getEmployee() {
    return employee;
  }


  @Override
  public Long getType() {
    return ItemType.DESK.getTypeId();
  }

  /**
   * Sets employee for desk and desk for employee.
   *
   * @param  employee employee to set
   */
  public void setEmployee(Employee employee) {

    if (this.employee != null) {
      this.employee.setDesk(null);
    }

    if (employee != null) {
      employee.setDesk(this);
    }

    this.employee = employee;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Desk)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Desk desk = (Desk) o;
    return Objects.equals(getEmployee(), desk.getEmployee())
        && Objects.equals(getType(), desk.getType());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), getEmployee(), getType());
  }

  @Override
  public String toString() {
    return "Desk{"
        + ", employee=" + employee
        + '}';
  }
}
