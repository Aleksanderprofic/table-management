package com.gd.tablesmanagement.filters.security;

import com.gd.tablesmanagement.persistance.model.RoleType;
import com.gd.tablesmanagement.services.security.AuthenticationUserDetails;
import com.gd.tablesmanagement.services.security.SecurityService;
import java.io.IOException;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class BearerAuthenticationFilter extends OncePerRequestFilter {

  private String tokenIntrospectEndpointUrl;

  private static final String AUTHORIZATION_TOKEN_HEADER = "Authorization";
  private static final String BEARER_TOKEN_PREFIX = "Bearer ";
  private static final int BEARER_TOKEN_PREFIX_LENGTH = 7;
  private static final String ACCESS_TOKEN_PARAM_NAME = "access_token";

  private static final long TIMESTAMP_MULTIPLIER = 1000;

  private static final String COULD_NOT_AUTHORIZE_WITH_BEARER_TOKEN_MESSAGE =
      "Could not verify access token";

  private final RestTemplate restTemplate;
  private final SecurityService securityService;

  /** Filter for authorizing requests with bearer Google OAuth token's from headers
   * ("Authorization" : "Bearer ${token}").
   *
   * @param restTemplate template for rest calls
   * @param securityService service for checking user security information
   * @param googleIntrospectEndpoint url of endpoint meant for checking information about token
   */
  public BearerAuthenticationFilter(
      RestTemplate restTemplate,
      SecurityService securityService,
      @Value("${google.token-introspect-endpoint}") String googleIntrospectEndpoint) {
    this.restTemplate = restTemplate;
    this.securityService = securityService;
    this.tokenIntrospectEndpointUrl = googleIntrospectEndpoint;
  }

  @Override
  public void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain chain)
      throws IOException, ServletException {

    String authHeader = request.getHeader(AUTHORIZATION_TOKEN_HEADER);

    if (authorizationHeaderContainsBearerToken(authHeader)) {
      authenticateIntrospectingBearerToken(authHeader.substring(BEARER_TOKEN_PREFIX_LENGTH));
    }

    chain.doFilter(request, response);

  }

  private void authenticateIntrospectingBearerToken(String token) {

    try {

      TokenInfoDto tokenInfoDto = getIntrospectInformationAboutToken(token);

      if (!isValid(tokenInfoDto)) {
        return;
      }

      String userEmail = tokenInfoDto.getEmail();

      Set<RoleType> userRoles = securityService.getUserRoles(userEmail);

      if (userRoles == null || userRoles.isEmpty()) {
        return;
      }

      saveUserDetailsIntoContext(token, userEmail, userRoles);

    } catch (Exception ex) {
      logger.debug(COULD_NOT_AUTHORIZE_WITH_BEARER_TOKEN_MESSAGE, ex);
    }
  }

  private void saveUserDetailsIntoContext(String token, String userEmail, Set<RoleType> userRoles) {

    try {

      AuthenticationUserDetails user = new AuthenticationUserDetails(userEmail, userRoles);

      BearerAuthenticationToken authentication =
          new BearerAuthenticationToken(token, user, user.getAuthorities());
      authentication.setAuthenticated(true);

      SecurityContextHolder.getContext().setAuthentication(authentication);

    } catch (Exception ex) {
      logger.debug(COULD_NOT_AUTHORIZE_WITH_BEARER_TOKEN_MESSAGE, ex);
    }
  }

  private TokenInfoDto getIntrospectInformationAboutToken(String token) {

    UriComponentsBuilder builder = UriComponentsBuilder
        .fromHttpUrl(tokenIntrospectEndpointUrl)
        .queryParam(ACCESS_TOKEN_PARAM_NAME, token);

    ResponseEntity<TokenInfoDto> googleResponse = restTemplate.exchange(
        builder.toUriString(),
        HttpMethod.GET,
        new HttpEntity<>(new HttpHeaders()),
        TokenInfoDto.class);

    return googleResponse.getBody();

  }

  /**
   * Verifies if tokenInfo for given google token match expected.
   *
   * @param tokenInfo introspect token information from google
   */
  private boolean isValid(TokenInfoDto tokenInfo) {

    if (tokenInfo == null) {
      return false;
    }

    long exp = tokenInfo.getExpiresTimestamp();
    return isExpiresTimestampValid(exp);

  }

  private boolean isExpiresTimestampValid(long expiresTimestamp) {

    return expiresTimestamp * TIMESTAMP_MULTIPLIER > System.currentTimeMillis();
  }

  private boolean authorizationHeaderContainsBearerToken(String authHeader) {
    return authHeader != null && authHeader.startsWith(BEARER_TOKEN_PREFIX);
  }
}
