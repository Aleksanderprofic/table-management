package com.gd.tablesmanagement.filters.security;

import com.gd.tablesmanagement.services.security.AuthenticationUserDetails;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class BearerAuthenticationToken extends AbstractAuthenticationToken {

  private final String token;
  private final AuthenticationUserDetails principal;

  /** Class representing Authentication using bearer token.
   *
   * @param token the token
   * @param principal user details
   * @param authorities user's authorities
   */
  public BearerAuthenticationToken(
      String token,
      AuthenticationUserDetails principal,
      Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    this.token = token;
    this.principal = principal;
  }

  @Override
  public Object getCredentials() {
    return token;
  }

  @Override
  public Object getPrincipal() {
    return principal;
  }

}
