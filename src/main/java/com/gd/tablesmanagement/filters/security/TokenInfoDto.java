package com.gd.tablesmanagement.filters.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenInfoDto {

  @JsonProperty("iss")
  private String issuerId;

  @JsonProperty("azp")
  private String requestingClientId;

  @JsonProperty("aud")
  private String audienceClientId;

  @JsonProperty("sub")
  private String uniqueUserId;

  @JsonProperty("scope")
  private String scopes;

  @JsonProperty("exp")
  private Long expiresTimestamp;

  @JsonProperty("email")
  private String email;

  @JsonProperty("expires_in")
  private String expiresIn;

  @JsonProperty("access_type")
  private String accessType;

  @Override
  public String toString() {
    return "TokenInfoDto{"
        + "issuerId='" + issuerId + '\''
        + ", requestingClientId='" + requestingClientId + '\''
        + ", audienceClientId='" + audienceClientId + '\''
        + ", uniqueUserId='" + uniqueUserId + '\''
        + ", scopes='" + scopes + '\''
        + ", expiresTimestamp=" + expiresTimestamp
        + ", email='" + email + '\''
        + ", expiresIn='" + expiresIn + '\''
        + ", accessType='" + accessType + '\''
        + '}';
  }

  public String getIssuerId() {
    return issuerId;
  }

  public void setIssuerId(String issuerId) {
    this.issuerId = issuerId;
  }

  public String getRequestingClientId() {
    return requestingClientId;
  }

  public void setRequestingClientId(String requestingClientId) {
    this.requestingClientId = requestingClientId;
  }

  public String getAudienceClientId() {
    return audienceClientId;
  }

  public void setAudienceClientId(String audienceClientId) {
    this.audienceClientId = audienceClientId;
  }

  public String getUniqueUserId() {
    return uniqueUserId;
  }

  public void setUniqueUserId(String uniqueUserId) {
    this.uniqueUserId = uniqueUserId;
  }

  public String getScopes() {
    return scopes;
  }

  public void setScopes(String scopes) {
    this.scopes = scopes;
  }

  public Long getExpiresTimestamp() {
    return expiresTimestamp;
  }

  public void setExpiresTimestamp(Long expiresTimestamp) {
    this.expiresTimestamp = expiresTimestamp;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(String expiresIn) {
    this.expiresIn = expiresIn;
  }

  public String getAccessType() {
    return accessType;
  }

  public void setAccessType(String accessType) {
    this.accessType = accessType;
  }
}
