package com.gd.tablesmanagement.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Component
@Order(1)
public class RequestAndResponseLoggingFilter extends OncePerRequestFilter {

  private static final Logger logger = LoggerFactory.getLogger(
      RequestAndResponseLoggingFilter.class);

  @Override
  protected void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain filterChain)
      throws ServletException, IOException {
    if (isAsyncDispatch(request)) {
      filterChain.doFilter(request, response);
    } else {
      doFilterWrapped(wrapRequest(request), wrapResponse(response), filterChain);
    }
  }

  private void doFilterWrapped(
      ContentCachingRequestWrapper request,
      ContentCachingResponseWrapper response,
      FilterChain filterChain)
      throws ServletException, IOException {

    logRequest(request);
    try {
      filterChain.doFilter(request, response);
    } finally {
      logResponse(request, response);
      response.copyBodyToResponse();
    }
  }

  private static void logRequest(ContentCachingRequestWrapper request) {
    logger.trace("{} {}", request.getMethod(), request.getRequestURI());
  }

  private static void logResponse(
      ContentCachingRequestWrapper request,
      ContentCachingResponseWrapper response) {
    int status = response.getStatus();
    logger.trace("{} {} <-- {} {}",
        status,
        HttpStatus.valueOf(status).getReasonPhrase(),
        request.getMethod(),
        request.getRequestURI());
  }

  private static ContentCachingRequestWrapper wrapRequest(HttpServletRequest request) {
    if (request instanceof ContentCachingRequestWrapper) {
      return (ContentCachingRequestWrapper) request;
    } else {
      return new ContentCachingRequestWrapper(request);
    }
  }

  private static ContentCachingResponseWrapper wrapResponse(HttpServletResponse response) {
    if (response instanceof ContentCachingResponseWrapper) {
      return (ContentCachingResponseWrapper) response;
    } else {
      return new ContentCachingResponseWrapper(response);
    }
  }
}
