package com.gd.tablesmanagement.services.persistance;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.FloorItem;
import com.gd.tablesmanagement.persistance.model.FloorItemPK;
import com.gd.tablesmanagement.persistance.model.RoomDescription;
import com.gd.tablesmanagement.persistance.repositories.FloorRepository;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionalDbOperations {

  private final FloorRepository floorRepository;

  /**  Service for doing data base operations with new transactions.
   * @param floorRepository  floorRepository
   */
  public TransactionalDbOperations(
      FloorRepository floorRepository) {
    this.floorRepository = floorRepository;
  }

  /** Updates floor with given id with data contained in another floor.
   *
   * @param floorId id of floor to update
   * @param data Floor containing data to update
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void updateFloorById(
      Long floorId,
      Floor data) {

    Floor toUpdate = floorRepository
        .findFloorById(floorId)
        .orElseThrow(IllegalArgumentException::new);

    toUpdate.setName(data.getName());
    toUpdate.setWalls(data.getWalls());

    toUpdate.setDesks(mergeItems(
        toUpdate,
        toUpdate.getDesks(),
        data.getDesks(),
        this::mergeDesk
        ));

    toUpdate.setRoomDescriptions(mergeItems(
        toUpdate,
        toUpdate.getRoomDescriptions(),
        data.getRoomDescriptions(),
        this::mergeDescription));

  }

  private <T extends FloorItem> List<T> mergeItems(
      Floor floor,
      List<T> oldItems,
      List<T> newItems,
      BiFunction<T, T, T> mergeFunction) {

    Map<FloorItemPK, T> dbItemsMap = oldItems
        .stream()
        .collect(Collectors.toMap(FloorItem::getIdKey, d -> d));

    newItems.forEach(d -> d.setFloor(floor));

    return newItems
        .stream()
        .map(item -> {
          T dbAnalogue = dbItemsMap.get(item.getIdKey());
          if (dbAnalogue == null) {
            return item;
          }
          return mergeFunction.apply(dbAnalogue, item);
        })
        .collect(Collectors.toList());
  }

  private <T extends FloorItem> void mergeItem(T dbItem, T newItem) {

    dbItem.setDescription(newItem.getDescription());
    dbItem.setRotation(newItem.getRotation());
    dbItem.setCoordinateX(newItem.getCoordinateX());
    dbItem.setCoordinateY(newItem.getCoordinateY());
  }

  private Desk mergeDesk(Desk dbDesk, Desk newDesk) {

    mergeItem(dbDesk, newDesk);
    dbDesk.setEmployee(newDesk.getEmployee());

    return dbDesk;
  }

  private RoomDescription mergeDescription(
      RoomDescription dbRoomDescription,
      RoomDescription newRoomDescription) {

    mergeItem(dbRoomDescription, newRoomDescription);
    dbRoomDescription.setName(newRoomDescription.getName());

    return dbRoomDescription;
  }

}
