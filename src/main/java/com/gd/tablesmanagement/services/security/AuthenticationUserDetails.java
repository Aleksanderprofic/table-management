package com.gd.tablesmanagement.services.security;

import com.gd.tablesmanagement.persistance.model.RoleType;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class AuthenticationUserDetails {

  private String email;

  private Set<RoleType> userRoles;

  public AuthenticationUserDetails(String email,
      Set<RoleType> useRoles) {
    this.email = email;
    this.userRoles = useRoles;
  }

  public String getEmail() {
    return email;
  }

  private void setEmail(String email) {
    this.email = email;
  }

  public Set<RoleType> getUserRoles() {
    return userRoles;
  }

  public void setUseRoles(Set<RoleType> useRoles) {
    this.userRoles = useRoles;
  }

  /** Returns user's authorities.
   *
   * @return user's authorities
   */
  public Set<SimpleGrantedAuthority> getAuthorities() {

    if (userRoles == null) {
      return Collections.emptySet();
    }

    return userRoles
        .stream()
        .map(RoleType::getAsSimpleGrantedAuthority)
        .collect(Collectors.toSet());
  }

  @Override
  public String toString() {
    return "AuthenticationUserDetails{"
        + "email='" + email + '\''
        + ", userRoles=" + userRoles
        + '}';
  }
}

