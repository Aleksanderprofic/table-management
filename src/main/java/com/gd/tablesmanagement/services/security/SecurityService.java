package com.gd.tablesmanagement.services.security;

import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.persistance.model.Role;
import com.gd.tablesmanagement.persistance.model.RoleType;
import com.gd.tablesmanagement.persistance.repositories.EmployeeRepository;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecurityService {

  private final EmployeeRepository employeeRepository;

  public SecurityService(
      EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  public boolean isInDatabase(String email) {
    Optional<Employee> employee = employeeRepository.findByEmail(email);
    return employee.isPresent();
  }

  /**
   * Checks if user exists and has admin privileges.
   *
   * @param email  user's email
   * @return  true if user's email is in database and user has admin privileges
   */
  public boolean existsAndHasAdminRole(String email) {
    Optional<Employee> employee = employeeRepository.findByEmail(email);
    Role role = new Role();
    role.setRoleType(RoleType.ADMIN);

    return employee.map(value -> value.getRoles().contains(role)).orElse(false);
  }

  /**
   * Checks if user exists and has regular privileges.
   *
   * @param email  user's email
   * @return  true if user's email is in database and user has regular privileges
   */
  public boolean existsAndHasRegularRole(String email) {
    Optional<Employee> employee = employeeRepository.findByEmail(email);
    Role role = new Role();
    role.setRoleType(RoleType.REGULAR);

    return employee.map(value -> value.getRoles().contains(role)).orElse(false);
  }

  /** Gets set of all roles of user with given email.
   *
   * @param userEmail  user mail
   * @return set of user's roles
   */
  @Transactional
  public Set<RoleType> getUserRoles(String userEmail) {

    Optional<Employee> employee = employeeRepository.findByEmail(userEmail);

    return employee
        .map(value -> value.getRoles()
            .stream()
            .map(Role::getRoleType)
            .collect(Collectors.toSet()))
        .orElseGet(HashSet::new);

  }
}
