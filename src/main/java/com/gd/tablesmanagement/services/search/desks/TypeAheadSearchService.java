package com.gd.tablesmanagement.services.search.desks;

import com.gd.tablesmanagement.services.search.desks.model.Trie;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TypeAheadSearchService<E> {

  private static final Logger logger = LoggerFactory.getLogger(TypeAheadSearchService.class);

  private final ExecutorService executorService = Executors.newCachedThreadPool();

  private final Trie<E> trie;

  private final ReadWriteLock trieLock = new ReentrantReadWriteLock(true);

  /** Service for concurrent updating and searching using custom trie,
   *  case and whitespace insensitive.
   *
   * @param keyExtractor  function that retrieves a string that objects are searched for.
   */
  public TypeAheadSearchService(Function<E, String> keyExtractor) {
    this.trie = new Trie<>(keyExtractor.andThen(
        s -> s == null
            ? s
            : s.trim().toLowerCase()));
  }

  public void updateSearchingData(List<E> data) {
    executorService.submit(() -> recreateSearchingData(data));
  }

  /** Get all objects that match given query.
   *
   * @param word  search query
   * @return completable future of sorted objects matching query.
   */
  public CompletableFuture<LinkedHashSet<E>> getSearchResult(String word) {

    CompletableFuture<LinkedHashSet<E>> completableFuture = new CompletableFuture<>();

    executorService.submit(() -> completableFuture.complete(search(word)));

    return completableFuture;
  }

  private void recreateSearchingData(List<E> data) {

    trieLock.writeLock().lock();

    try {

      trie.clear();
      data.forEach(trie::insert);
      logger.debug("Created search indexes for {} desks", data.size());

    } catch (Exception ex) {
      logger.debug(ex.getMessage(), ex);
    } finally {
      trieLock.writeLock().unlock();
    }
  }

  private LinkedHashSet<E> search(String word) {

    trieLock.readLock().lock();

    LinkedHashSet<E> loadedSet;

    try {

      loadedSet = trie.findAll(word.trim().toLowerCase());

    } catch (Exception ex) {

      logger.debug(ex.getMessage(), ex);
      loadedSet = new LinkedHashSet<>();

    } finally {

      trieLock.readLock().unlock();

    }

    logger.debug("Found {} desks matching query \"{}\"", loadedSet.size(), word);
    return loadedSet;
  }

}
