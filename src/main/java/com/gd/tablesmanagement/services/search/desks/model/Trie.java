package com.gd.tablesmanagement.services.search.desks.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Trie<E> {

  private TrieNode<E> root = new TrieNode<>();
  private Function<E, String> keyExtractor;
  private ConcurrentHashMap<Character, HashSet<TrieNode<E>>> allNodes = new ConcurrentHashMap<>();

  public Trie(Function<E, String> keyExtractor) {
    this.keyExtractor = keyExtractor;
  }

  /** Inserts object into trie using string retrieved with key extractor..
   *
   * @param objectToInsert  object to insert
   */
  public void insert(E objectToInsert) {

    TrieNode<E> current = root;
    String word = keyExtractor.apply(objectToInsert);

    if (word == null) {
      return;
    }

    for (int i = 0; i < word.length(); i++) {
      current = current
          .getChildren()
          .computeIfAbsent(word.charAt(i), c -> new TrieNode<>());

      allNodes.putIfAbsent(word.charAt(i), new HashSet<>());
      allNodes.get(word.charAt(i)).add(current);
    }

    current.addContent(objectToInsert);
  }

  /** Returns all objects in trie that contain given key.
   *
   * @param word  key to search for
   * @return all objects that match given key sorted by field retrieved with key extractor.
   */
  public LinkedHashSet<E> findAll(String word) {

    if ("".equals(word)) {
      return new LinkedHashSet<>(findForRoot(root, word));
    }

    return allNodes
        .get(word.charAt(0))
        .stream()
        .flatMap(root -> findForRoot(root, word.substring(1)).stream())
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  private List<E> findForRoot(TrieNode<E> current, String word) {

    for (int i = 0; i < word.length(); i++) {

      char ch = word.charAt(i);
      TrieNode<E> node = current.getChildren().get(ch);
      if (node == null) {
        return Collections.emptyList();
      }
      current = node;
    }

    return getTreeContent(current);
  }

  private List<E> getTreeContent(TrieNode<E> root) {
    List<E> resultsFromChildren = root
        .getChildren()
        .values()
        .stream()
        .flatMap(node -> getTreeContent(node).stream())
        .collect(Collectors.toList());

    resultsFromChildren.addAll(root.getContent());

    return resultsFromChildren;
  }

  public void clear() {
    root = new TrieNode<>();
    allNodes = new ConcurrentHashMap<>();
  }
}
