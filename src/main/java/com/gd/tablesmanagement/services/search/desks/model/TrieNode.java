package com.gd.tablesmanagement.services.search.desks.model;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TrieNode<E> {

  private ConcurrentHashMap<Character, TrieNode<E>> children = new ConcurrentHashMap<>();
  private List<E> content = new LinkedList<>();

  public ConcurrentHashMap<Character, TrieNode<E>> getChildren() {
    return children;
  }

  public void addContent(E newContent) {
    content.add(newContent);
  }

  public void removeContent(E oldContent) {
    content.remove(oldContent);
  }

  public List<E> getContent() {
    return content;
  }
}
