package com.gd.tablesmanagement.services.google.sheets.reader;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.gd.tablesmanagement.services.google.sheets.floors.GdFloor;
import com.gd.tablesmanagement.services.google.sheets.processors.SheetProcessor;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GoogleSheetsReader {

  private static final Logger logger = LoggerFactory.getLogger(GoogleSheetsReader.class);

  private final SheetsService sheetsService;
  private final List<SheetProcessor> processors;

  /**
   * Service for reading database entities from google sheets.
   *
   * @param sheetsService  service for reading raw data from google sheets.
   * @param processors  services for reading data from particular google sheet grid
   */
  public GoogleSheetsReader(
      SheetsService sheetsService,
      List<SheetProcessor> processors) {

    this.sheetsService = sheetsService;
    this.processors = processors;

  }

  /**
   *  Gets all desks parsed for default google sheet formatting.
   *
   * @param sheet  sheet to load data from
   * @return list of desks
   */
  public List<Desk> findAllDesks(SheetIdentity sheet) {

    List<Desk> retrievedDesks = processors.stream()
        .flatMap(p -> getDesksFromProcessor(p, sheet).stream())
        .collect(Collectors.toList());

    logger.debug("Found {} desks in spreadsheet", retrievedDesks.size());

    return retrievedDesks;
  }

  /** Looks up all desks for processors that process given floor.
   * @param sheet  spreadsheet to look in.
   * @param floor  floor enum
   * @return list of parsed desks from given floor
   */
  public List<Desk> findAllDesksForFloor(SheetIdentity sheet, GdFloor floor) {

    List<Desk> retrievedDesks = processors.stream()
        .filter(p -> p.isPartOfGdFloor(floor))
        .flatMap(p -> getDesksFromProcessor(p, sheet).stream())
        .collect(Collectors.toList());

    logger.debug("Found {} desks in spreadsheet", retrievedDesks.size());

    return retrievedDesks;
  }

  /**
   *  Saves desk for default google sheet formatting.
   *
   * @param sheet  sheet to save data to
   * @param deskToSave  desk saved to google sheet by it's internalId
   * @return response from google if internalId exists in sheet
   * @throws IOException when thrown during data reading/saving
   */
  public Optional<UpdateValuesResponse> saveDesk(SheetIdentity sheet, Desk deskToSave)
      throws IOException {

    Long internalId = deskToSave.getGridOfficeId();

    for (SheetProcessor processor : processors) {
      Optional<CellPosition> searchResults = processor
          .findCellPositionForTableId(sheet, internalId);

      if (searchResults.isPresent()) {

        logger.debug("Found desk id {} at {}", internalId, searchResults.get().getAsString());

        return Optional.of(saveCellsToSheet(
            sheet,
            searchResults.get(),
            processor.getColumns(),
            deskToSave));
      }
    }

    logger.debug("Could not find desk id {}", internalId);

    return Optional.empty();
  }

  private UpdateValuesResponse saveCellsToSheet(
      SheetIdentity sheet,
      CellPosition startingPosition,
      List<ColumnType> columnsToSave,
      Desk deskToSave)
      throws IOException {

    GridRange updateRange = new GridRange(startingPosition,
        startingPosition.plusColumns(columnsToSave.size()));
    GridValues gridToSave = GridValues.fromDesk(deskToSave, columnsToSave);

    UpdateValuesResponse response = sheetsService.updateSpreadSheet(sheet, updateRange, gridToSave);

    logger.debug("Updated {} cells in spreadsheet", response.getUpdatedCells());

    return response;

  }

  private List<Desk> getDesksFromProcessor(SheetProcessor processor, SheetIdentity sheet) {
    try {
      return processor.findDesks(sheet);
    } catch (IOException ex) {
      logger.debug(ex.getMessage(), ex);
      return Collections.emptyList();
    }
  }
}
