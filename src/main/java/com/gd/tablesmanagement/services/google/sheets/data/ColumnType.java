package com.gd.tablesmanagement.services.google.sheets.data;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;

public enum ColumnType {

  DESK_ID {
    @Override
    public void updateDeskFromCell(Desk table, Object data) {

      if (data instanceof Number) {
        table.setGridOfficeId(((Number) data).longValue());
      } else if (data instanceof String) {
        table.setGridOfficeId(Long.parseLong((String) data));
      } else {
        throw new IllegalArgumentException("Could not parse object");
      }
    }

    @Override
    public Object getCellValueFromDesk(Desk table) {
      return table.getGridOfficeId();
    }
  },

  EMPLOYEE_NAME {
    @Override
    public void updateDeskFromCell(Desk table, Object data) {

      if (!(data instanceof String)) {
        throw new IllegalArgumentException("Could not parse object");
      }

      Employee employee = table.getEmployee();

      if (employee == null) {
        employee = new Employee();
        table.setEmployee(employee);
      }
      employee.setName((String) data);

    }

    @Override
    public Object getCellValueFromDesk(Desk table) {

      if (table == null) {
        return null;
      }
      Employee employee = table.getEmployee();
      if (employee == null) {
        return null;
      }
      return employee.getName();
    }
  },

  EMPLOYEE_ACCOUNT {
    @Override
    public void updateDeskFromCell(Desk table, Object data) {

      if (!(data instanceof String)) {
        throw new IllegalArgumentException("Could not parse object");
      }

      Employee employee = table.getEmployee();

      if (employee == null) {
        employee = new Employee();
        table.setEmployee(employee);
      }
      employee.setAccount((String) data);

    }

    @Override
    public Object getCellValueFromDesk(Desk table) {

      if (table == null) {
        return null;
      }

      Employee employee = table.getEmployee();
      if (employee == null) {
        return null;
      }
      return employee.getAccount();
    }
  },

  EMPTY {
    @Override
    public void updateDeskFromCell(Desk table, Object data) {
    }

    @Override
    public Object getCellValueFromDesk(Desk table) {
      return "";
    }
  };

  public abstract void updateDeskFromCell(Desk table, Object data);

  public abstract Object getCellValueFromDesk(Desk table);
}

