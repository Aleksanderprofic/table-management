package com.gd.tablesmanagement.services.google.sheets.data;

import com.google.common.base.Objects;

public final class SheetIdentity {

  private final String spreadsheetId;
  private final String sheetName;

  public SheetIdentity(String spreadsheetId, String sheetName) {
    this.spreadsheetId = spreadsheetId;
    this.sheetName = sheetName;
  }

  public String getSpreadsheetId() {
    return spreadsheetId;
  }

  public String getSheetName() {
    return sheetName;
  }

  @Override
  public String toString() {
    return spreadsheetId + ":" + sheetName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SheetIdentity)) {
      return false;
    }
    SheetIdentity that = (SheetIdentity) o;
    return Objects.equal(getSpreadsheetId(), that.getSpreadsheetId())
        && Objects.equal(getSheetName(), that.getSheetName());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getSpreadsheetId(), getSheetName());
  }
}
