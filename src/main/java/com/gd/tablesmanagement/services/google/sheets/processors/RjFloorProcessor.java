package com.gd.tablesmanagement.services.google.sheets.processors;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.gd.tablesmanagement.services.google.sheets.floors.GdFloor;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class RjFloorProcessor extends SheetProcessor {

  private static final String RJ_ACCOUNT_STRING = "RJ";

  public RjFloorProcessor(
      SheetsService sheetsService,
      @Value("${google.sheets.defaults.rj-floor-start}") CellPosition gridStart,
      @Value("${google.sheets.defaults.rj-floor-end}") CellPosition gridEnd,
      @Value("${google.sheets.defaults.rj-floor-columns}") List<ColumnType> columns) {

    super(sheetsService, new GridRange(gridStart, gridEnd), columns);
  }

  @Override
  public boolean isPartOfGdFloor(GdFloor floor) {

    return floor.equals(GdFloor.THIRD_FLOOR);

  }

  @Override
  public List<Desk> findDesks(SheetIdentity sheet) throws IOException {
    List<Desk> retrievedDesks = super.findDesks(sheet);
    retrievedDesks.forEach(desk -> {
      Employee employee = desk.getEmployee();

      if (employee == null) {
        employee = new Employee();
        desk.setEmployee(employee);
      }
      employee.setAccount(RJ_ACCOUNT_STRING);
    });

    return retrievedDesks;
  }
}

