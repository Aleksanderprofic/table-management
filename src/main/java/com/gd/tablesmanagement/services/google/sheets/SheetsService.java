package com.gd.tablesmanagement.services.google.sheets;

import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import java.io.IOException;
import org.springframework.stereotype.Service;

@Service
public class SheetsService {

  private static final String RAW_UPDATE_OPTION_STRING = "RAW";
  private final Sheets sheets;

  /**
   * Service for reading raw data from google sheets.
   *
   * @param sheets Google sheets service
   */
  public SheetsService(Sheets sheets) {
    this.sheets = sheets;
  }

  /**
   * Loads data from given google sheet.
   *
   * @param sheet  sheet to load data from
   * @param range  grid range to load
   * @return GridValues from given range
   * @throws IOException when thrown during data reading/saving
   */
  public GridValues loadSpreadSheet(SheetIdentity sheet, GridRange range) throws IOException {

    String sheetId = sheet.getSpreadsheetId();
    String rangeString = range.getRangeString(sheet);

    ValueRange loadedValues = sheets
        .spreadsheets()
        .values()
        .get(sheetId, rangeString)
        .execute();

    return new GridValues(loadedValues);
  }

  /**
   * Saves given grid to google sheet.
   *
   * @param sheet  sheet to save data to
   * @param range  GridRange to save data to
   * @param gridToSave  data to save
   * @return UpdateValuesResponse from google's API
   * @throws IOException when thrown during data reading/saving
   */
  public UpdateValuesResponse updateSpreadSheet(
      SheetIdentity sheet,
      GridRange range,
      GridValues gridToSave)
      throws IOException {

    String sheetId = sheet.getSpreadsheetId();
    String rangeString = range.getRangeString(sheet);

    return sheets.spreadsheets()
        .values()
        .update(sheetId, rangeString, gridToSave.getValueRange())
        .setValueInputOption(RAW_UPDATE_OPTION_STRING)
        .execute();
  }
}