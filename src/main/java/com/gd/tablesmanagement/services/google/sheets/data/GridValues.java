package com.gd.tablesmanagement.services.google.sheets.data;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GridValues {

  private static final Logger logger = LoggerFactory.getLogger(GridValues.class);

  private final ValueRange valueRange;

  public GridValues(ValueRange valueRange) {
    this.valueRange = valueRange;
  }

  public GridValues(List<List<Object>> grid) {
    valueRange = new ValueRange();
    valueRange.setValues(grid);
  }

  /**
   * Returns GridValues containing row representing given desk formatted by column types.
   *
   * @param desk  desk to be parsed
   * @param columns  ordered columns representing row format
   * @return GridValues containing row representing given desk
   */
  public static GridValues fromDesk(Desk desk, List<ColumnType> columns) {

    List<Object> valuesToSave = columns
        .stream()
        .map(column -> column.getCellValueFromDesk(desk))
        .collect(Collectors.toList());

    return new GridValues(new ValueRange().setValues(Collections.singletonList(valuesToSave)));

  }

  public List<List<Object>> getValues() {
    return valueRange.getValues();
  }

  public ValueRange getValueRange() {
    return valueRange;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof GridValues)) {
      return false;
    }
    GridValues that = (GridValues) o;
    return Objects.equal(valueRange, that.valueRange);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(valueRange);
  }

  /**
   * Parses GridValues into Desk entities by format given as column types.
   *
   * @param columns  ordered columns representing row format
   * @return  list of parsed desks
   */
  public List<Desk> parseToDesksByColumns(List<ColumnType> columns) {

    return valueRange
        .getValues()
        .stream()
        .map(row -> parseRowByColumns(row, columns))
        .collect(Collectors.toList());
  }

  private Desk parseRowByColumns(List<Object> row, List<ColumnType> columns) {

    Desk desk = new Desk();
    for (int i = 0; i < row.size() && i < columns.size(); i++) {
      try {
        columns.get(i).updateDeskFromCell(desk, row.get(i));
      } catch (IllegalArgumentException ex) {
        logger.debug(ex.getMessage(), ex);
      }
    }
    return desk;
  }

}
