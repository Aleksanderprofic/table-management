package com.gd.tablesmanagement.services.google.sheets.processors;

import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.floors.GdFloor;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FirstFloorSheetProcessor extends SheetProcessor {

  public FirstFloorSheetProcessor(
      SheetsService sheetsService,
      @Value("${google.sheets.defaults.first-floor-start}") CellPosition gridStart,
      @Value("${google.sheets.defaults.first-floor-end}") CellPosition gridEnd,
      @Value("${google.sheets.defaults.first-floor-columns}") List<ColumnType> columns) {

    super(sheetsService, new GridRange(gridStart, gridEnd), columns);
  }

  @Override
  public boolean isPartOfGdFloor(GdFloor floor) {

    return floor.equals(GdFloor.FIRST_FLOOR);

  }

}
