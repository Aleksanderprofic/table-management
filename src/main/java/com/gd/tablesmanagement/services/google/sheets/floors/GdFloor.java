package com.gd.tablesmanagement.services.google.sheets.floors;

public enum GdFloor {
  FIRST_FLOOR,
  THIRD_FLOOR;
}
