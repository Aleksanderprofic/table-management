package com.gd.tablesmanagement.services.google.sheets.data;

import com.google.common.base.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CellPosition {

  private static final int ALPHABET_LENGTH = 26;

  private static final String identityRegex = "([A-Z]+)([0-9]+)";
  private static final String columnRegex = "[A-Z]+";
  private static final Pattern wholeCellPattern = Pattern.compile(identityRegex);
  private static final Pattern columnPattern = Pattern.compile(columnRegex);
  private final String column;
  private final int row;

  /**
   * Immutable class representing cell position in excel spreadsheet.
   *
   * @param column  spreadsheet column matching [A-Z]+
   * @param row  spreadsheet row
   */
  public CellPosition(String column, int row) {
    Matcher matcher = columnPattern.matcher(column);
    if (!matcher.matches()) {
      throw new IllegalArgumentException(column + " is not a valid column identifier");
    }
    this.column = column;
    this.row = row;
  }

  /**
   * Class representing cell position in excel spreadsheet.
   *
   * @param positionString  spreadsheet cell position e.g. A13
   */
  public CellPosition(String positionString) {
    Matcher matcher = wholeCellPattern.matcher(positionString);
    if (!matcher.matches()) {
      throw new IllegalArgumentException(positionString + " is not a valid cell identifier");
    }
    this.column = matcher.group(1);
    this.row = Integer.parseInt(matcher.group(2));

  }

  public int getRow() {
    return row;
  }

  public String getColumn() {
    return column;
  }

  public String getAsString() {
    return column + row;
  }

  /**
   * Adds rows.
   *
   * @param additionalRows  number of rows to add
   * @return new CellPosition with rows added
   */
  public CellPosition plusRows(int additionalRows) {
    return new CellPosition(column, row + additionalRows);
  }

  /**
   * Adds columns.
   *
   * @param additionalColumns  number of columns to add
   * @return new CellPosition with columns added
   */
  public CellPosition plusColumns(int additionalColumns) {

    int columnNumber = getIntFromColumn(column);
    columnNumber += additionalColumns;

    return new CellPosition(getColumnFromInt(columnNumber), row);
  }

  private static String getColumnFromInt(int columnNumber) {

    StringBuilder columnName = new StringBuilder();

    while (columnNumber > 0) {
      int remainder = columnNumber % ALPHABET_LENGTH;

      if (remainder == 0) {
        columnName.append("Z");
        columnNumber = (columnNumber / ALPHABET_LENGTH) - 1;
      } else {
        columnName.append((char) ((remainder - 1) + 'A'));
        columnNumber = columnNumber / ALPHABET_LENGTH;
      }
    }

    return columnName.reverse().toString();
  }

  private static int getIntFromColumn(String columnName) {

    if (columnName == null || columnName.equals("")) {
      throw new IllegalArgumentException(columnName + " is not a valid column identifier");
    }

    char[] charColumnArray = columnName.toUpperCase().toCharArray();
    int sum = 0;

    for (int i = 0; i < columnName.length(); i++) {
      sum *= ALPHABET_LENGTH;
      sum += (charColumnArray[i] - 'A' + 1);
    }

    return sum;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CellPosition)) {
      return false;
    }
    CellPosition that = (CellPosition) o;
    return getRow() == that.getRow()
        && Objects.equal(getColumn(), that.getColumn());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getColumn(), getRow());
  }

  @Override
  public String toString() {
    return getAsString();
  }
}
