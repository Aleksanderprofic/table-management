package com.gd.tablesmanagement.services.google.sheets.data;

import com.google.common.base.Objects;

public final class GridRange {

  private final CellPosition start;
  private final CellPosition end;

  public GridRange(CellPosition start, CellPosition end) {
    this.start = start;
    this.end = end;
  }

  public CellPosition getStart() {
    return start;
  }

  public CellPosition getEnd() {
    return end;
  }

  /**
   * Returns String representing grid range for reading from google API.
   *
   * @param sheet  sheet from which data is read
   * @return String representing grid range in given sheet for google API
   */
  public String getRangeString(SheetIdentity sheet) {

    String fromString = start.getAsString();
    String toString = end.getAsString();
    String sheetName = sheet.getSheetName();

    return sheetName.equals("")
        ? String.format("%s:%s", fromString, toString)
        : String.format("%s!%s:%s", sheetName, fromString, toString);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof GridRange)) {
      return false;
    }
    GridRange gridRange = (GridRange) o;
    return Objects.equal(getStart(), gridRange.getStart())
        && Objects.equal(getEnd(), gridRange.getEnd());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getStart(), getEnd());
  }

  @Override
  public String toString() {
    return "GridRange{"
        + "start=" + start
        + ", end=" + end
        + '}';
  }
}
