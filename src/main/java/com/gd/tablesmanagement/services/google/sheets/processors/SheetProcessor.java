package com.gd.tablesmanagement.services.google.sheets.processors;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.gd.tablesmanagement.services.google.sheets.floors.GdFloor;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class SheetProcessor {

  private final GridRange gridRange;
  private final List<ColumnType> columns;
  private final SheetsService sheetsService;

  protected SheetProcessor(
      SheetsService sheetsService,
      GridRange gridRange,
      List<ColumnType> columns) {

    this.sheetsService = sheetsService;
    this.gridRange = gridRange;
    this.columns = columns;

  }

  public boolean isPartOfGdFloor(GdFloor floor) {
    return false;
  }

  /**
   *  Gets all desks from processor's gridRange parsed by processor's column types.
   *
   * @param sheet  sheet to load data from
   * @return all loaded Desk entities
   * @throws IOException when thrown during data reading/saving
   */
  public List<Desk> findDesks(SheetIdentity sheet) throws IOException {

    GridValues dataRetrievedFromSheet = sheetsService.loadSpreadSheet(sheet, gridRange);
    return dataRetrievedFromSheet.parseToDesksByColumns(columns);

  }

  /**
   * Searches for sheet position of internalId in processor's gridRange,
   * assumes internalId is in first column.
   *
   * @param sheet  sheet to load data from
   * @param deskInternalId  searched internalId
   * @return CellPosition of internalId if found
   * @throws IOException when thrown during data reading/saving
   */
  public Optional<CellPosition> findCellPositionForTableId(
      SheetIdentity sheet,
      Long deskInternalId)
      throws IOException {

    GridValues retrievedData = sheetsService.loadSpreadSheet(sheet, gridRange);

    if (retrievedData.getValues() == null) {
      return Optional.empty();
    }

    int startingRowIndex = gridRange.getStart().getRow();
    for (List<Object> row : retrievedData.getValues()) {
      if (row.size() > 0) {
        try {
          if (Integer.parseInt((String) row.get(0)) == deskInternalId) {
            return Optional
                .of(new CellPosition(gridRange.getStart().getColumn(), startingRowIndex));
          }
        } catch (ClassCastException | NumberFormatException ignore) {
          // We only search for one correct value
        }
      }
      startingRowIndex++;
    }
    return Optional.empty();
  }

  public List<ColumnType> getColumns() {
    return columns;
  }
}
