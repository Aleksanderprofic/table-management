package com.gd.tablesmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class TablesManagementApplication {

  public static void main(String[] args) {
    SpringApplication.run(TablesManagementApplication.class, args);
  }
}
