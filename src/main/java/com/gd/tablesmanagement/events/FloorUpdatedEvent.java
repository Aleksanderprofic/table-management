package com.gd.tablesmanagement.events;

public class FloorUpdatedEvent {

  private Long floorId;

  public FloorUpdatedEvent(Long floorId) {
    this.floorId = floorId;
  }

  public Long getFloorId() {
    return floorId;
  }

  public void setFloorId(Long floorId) {
    this.floorId = floorId;
  }

}
