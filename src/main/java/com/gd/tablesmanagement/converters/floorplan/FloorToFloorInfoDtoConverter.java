package com.gd.tablesmanagement.converters.floorplan;

import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorInfoDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorMetaData;
import com.gd.tablesmanagement.persistance.model.Floor;
import org.springframework.lang.NonNull;

public class FloorToFloorInfoDtoConverter {

  /**  Converts Floor to FloorInfoDto containing some floor meta information.
   *
   * @param from  floor to convert
   * @return  FloorInfoDto with corresponding floor information
   */
  public static @NonNull FloorInfoDto convert(@NonNull Floor from) {
    FloorInfoDto to = new FloorInfoDto();
    FloorMetaData toMetaData =  new FloorMetaData();

    toMetaData.setName(from.getName());

    to.setId(from.getId());
    to.setData(toMetaData);

    return to;

  }
}
