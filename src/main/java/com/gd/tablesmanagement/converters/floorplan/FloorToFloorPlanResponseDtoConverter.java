package com.gd.tablesmanagement.converters.floorplan;

import com.gd.tablesmanagement.controllers.dto.floorplan.CornerDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanResponseDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.ItemDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.WallDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorMetaData;
import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemType;
import com.gd.tablesmanagement.persistance.model.Corner;
import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.FloorItem;
import com.gd.tablesmanagement.persistance.model.RoomDescription;
import com.gd.tablesmanagement.persistance.model.Wall;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.lang.NonNull;

public class FloorToFloorPlanResponseDtoConverter {

  /**  Converts Floor to complete FloorPlanResponseDto object.
   *
   * @param from  floor to convert
   * @return  FloorPlanResponseDto with corresponding floor information
   */
  public static @NonNull FloorPlanResponseDto convert(@NonNull Floor from) {

    FloorPlanResponseDto to = new FloorPlanResponseDto();
    to.setId(from.getId());

    to.setFloorMetaData(getFloorMetaData(from));

    FloorPlanDto toPlanDto = new FloorPlanDto();

    toPlanDto.setWalls(getWallDtoListFromFloor(from.getWalls()));
    toPlanDto.setCorners(getCornerDtoListFromWallList(from.getWalls()));

    List<ItemDto> floorDesks = getItemDtoListFromDeskList(from.getDesks());
    List<ItemDto> floorDescriptions = getItemDtoListFromRoomDescriptionsList(
        from.getRoomDescriptions());

    toPlanDto.setItems(floorDesks);
    toPlanDto.addItems(floorDescriptions);

    to.setFloorPlanDto(toPlanDto);

    return to;
  }

  private static List<ItemDto> getItemDtoListFromRoomDescriptionsList(
      List<RoomDescription> roomDescriptions) {

    List<ItemDto> allItems = new LinkedList<>();
    for (RoomDescription description : roomDescriptions) {
      ItemDto convertedItem = convertFloorItemToItemDto(description);
      convertedItem = enrichItemDtoWithDescriptionSpecificInfo(convertedItem, description);
      allItems.add(convertedItem);
    }
    return allItems;

  }

  private static List<ItemDto> getItemDtoListFromDeskList(List<Desk> desks) {

    List<ItemDto> allItems = new LinkedList<>();
    for (Desk desk : desks) {
      ItemDto convertedItem = convertFloorItemToItemDto(desk);
      convertedItem = enrichItemDtoWithDeskSpecificInfo(convertedItem, desk);
      allItems.add(convertedItem);
    }
    return allItems;
  }

  //it's supposed to be hashset first as corners may and will repeat
  @SuppressWarnings("FuseStreamOperations")
  private static List<CornerDto> getCornerDtoListFromWallList(List<Wall> walls) {

    return new LinkedList<>(
        walls.stream()
        .flatMap(wall -> convertWallToCornerDtos(wall).stream())
        .collect(Collectors.toCollection(HashSet::new)));

  }

  private static List<WallDto> getWallDtoListFromFloor(List<Wall> walls) {
    return walls.stream()
        .map(FloorToFloorPlanResponseDtoConverter::convertWallToWallDto)
        .collect(Collectors.toList());
  }

  private static FloorMetaData getFloorMetaData(Floor from) {

    FloorMetaData toMetaData = new FloorMetaData();
    toMetaData.setName(from.getName());
    return toMetaData;

  }

  private static WallDto convertWallToWallDto(Wall from) {

    WallDto to = new WallDto();

    if (from == null) {
      return to;
    }

    if (from.getCorner1() != null) {
      to.setCorner1(from.getCorner1().getFrontendId());
    }

    if (from.getCorner2() != null) {
      to.setCorner2(from.getCorner2().getFrontendId());
    }

    return to;

  }

  private static List<CornerDto> convertWallToCornerDtos(Wall from) {

    List<CornerDto> to = new LinkedList<>();

    if (from == null) {
      return to;
    }

    Corner corner1 = from.getCorner1();
    Corner corner2 = from.getCorner2();

    if (corner1 != null) {

      CornerDto corner = new CornerDto();
      corner.setId(corner1.getFrontendId());
      corner.setCoordinateX(corner1.getCoordinateX());
      corner.setCoordinateY(corner1.getCoordinateY());
      to.add(corner);

    }

    if (corner2 != null) {

      CornerDto corner = new CornerDto();
      corner.setId(corner2.getFrontendId());
      corner.setCoordinateX(corner2.getCoordinateX());
      corner.setCoordinateY(corner2.getCoordinateY());
      to.add(corner);

    }

    return to;
  }

  private static ItemDto convertFloorItemToItemDto(FloorItem from) {

    ItemDto to = new ItemDto();

    if (from == null) {
      return to;
    }

    to.setCoordinateX(from.getCoordinateX());
    to.setCoordinateY(from.getCoordinateY());
    to.setDescription(from.getDescription());
    to.setRotation(from.getRotation());
    to.setId(from.getIdKey().getGridOfficeId());

    return to;
  }

  private static ItemDto enrichItemDtoWithDeskSpecificInfo(
      ItemDto to,
      Desk from) {

    if (from == null) {
      return to;
    }

    Employee fromEmployee = from.getEmployee();

    if (fromEmployee != null) {
      String employeeName = fromEmployee.getName();
      String accountName = fromEmployee.getAccount();

      to.setName(employeeName != null
          ? employeeName
          : "");
      to.setAccount(accountName != null
          ? accountName
          : "");
    } else {
      to.setName("");
      to.setAccount("");
    }

    to.setType(ItemType.DESK.getTypeId());

    return to;
  }

  private static ItemDto enrichItemDtoWithDescriptionSpecificInfo(
      ItemDto to,
      RoomDescription from) {

    if (from == null) {
      return to;
    }

    to.setType(ItemType.ROOM_DESCRIPTION.getTypeId());
    to.setName(from.getName());
    return to;
  }
}
