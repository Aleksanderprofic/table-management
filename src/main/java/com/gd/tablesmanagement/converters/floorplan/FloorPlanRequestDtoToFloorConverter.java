package com.gd.tablesmanagement.converters.floorplan;

import com.gd.tablesmanagement.controllers.dto.floorplan.CornerDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanRequestDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.ItemDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.WallDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemType;
import com.gd.tablesmanagement.persistance.model.Corner;
import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.FloorItem;
import com.gd.tablesmanagement.persistance.model.RoomDescription;
import com.gd.tablesmanagement.persistance.model.Wall;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.lang.NonNull;

public class FloorPlanRequestDtoToFloorConverter {

  /** Converts FloorPlanRequestDto to corresponding db Floor object.
   *
   * @param from  request to convert
   * @return  floor containing the same data
   */
  public static @NonNull Floor convert(@NonNull FloorPlanRequestDto from) {

    Floor to = new Floor();

    List<Desk> desks = convertItemDtoListToDeskList(from.getFloorPlanDto().getItems());
    List<RoomDescription> roomDescriptions = convertItemDtoListToRoomDescriptionList(
        from.getFloorPlanDto().getItems());

    List<Wall> walls = convertWallDtoListAndCornerDtoListToWallList(
        from.getFloorPlanDto().getCorners(),
        from.getFloorPlanDto().getWalls());

    to.setName(from.getFloorMetaData().getName());
    to.setDesks(desks);
    to.setWalls(walls);
    to.setRoomDescriptions(roomDescriptions);

    return to;
  }

  /** Converts List of ItemDtos to list of Desks.
   *
   * @param items  items to convert
   * @return all desks item list contains, parsed
   */
  public static @NonNull List<Desk> convertItemDtoListToDeskList(@NonNull List<ItemDto> items) {
    return items
        .stream()
        .filter(itemDto -> itemDto.getType().equals(ItemType.DESK.getTypeId()))
        .map(FloorPlanRequestDtoToFloorConverter::convertItemDtoToDesk)
        .collect(Collectors.toList());
  }

  /** Converts List of ItemDtos to list of RoomDescriptions.
   *
   * @param items  items to convert
   * @return all room descriptions item list contains, parsed
   */
  public static @NonNull List<RoomDescription> convertItemDtoListToRoomDescriptionList(
      @NonNull List<ItemDto> items) {
    return items
        .stream()
        .filter(itemDto -> itemDto.getType().equals(ItemType.ROOM_DESCRIPTION.getTypeId()))
        .map(FloorPlanRequestDtoToFloorConverter::convertItemDtoToRoomDescription)
        .collect(Collectors.toList());
  }


  /** Converts List of WallDtos and CornerDto to List of corresponding Wall entities with Corners
   *  assigned to them.
   *
   * @param cornerDtoList  list of corners of the walls
   * @param wallDtoList  list of walls
   * @return parsed walls with mapped corners
   */
  public static @NonNull List<Wall> convertWallDtoListAndCornerDtoListToWallList(
      @NonNull List<CornerDto> cornerDtoList,
      @NonNull List<WallDto> wallDtoList) {

    Map<String, Corner> allCorners = cornerDtoList
        .stream()
        .map(FloorPlanRequestDtoToFloorConverter::convertCornerDtoToCorner)
        .collect(Collectors.toMap(Corner::getFrontendId, c -> c));

    return wallDtoList
        .stream()
        .map(wallDto -> convertCornersToWall(
            wallDto,
            allCorners.get(wallDto.getCorner1()),
            allCorners.get(wallDto.getCorner2())
        ))
        .collect(Collectors.toList());
  }

  private static Desk convertItemDtoToDesk(
      ItemDto from) {

    Desk to = updateFloorItemWithItemDto(from, new Desk());
    Employee employee = new Employee();
    to.setEmployee(employee);

    if (from == null) {
      return to;
    }

    employee.setAccount(from.getAccount());
    employee.setName(from.getName());

    return to;
  }

  private static RoomDescription convertItemDtoToRoomDescription(
      ItemDto from) {

    RoomDescription to = updateFloorItemWithItemDto(from, new RoomDescription());

    if (from == null) {
      return to;
    }

    to.setName(from.getName());

    return to;
  }

  private static <T extends FloorItem> T updateFloorItemWithItemDto(ItemDto from, T to) {

    if (from == null) {
      return to;
    }

    to.setDescription(from.getDescription());
    to.setRotation(from.getRotation());
    to.setDescription(from.getDescription());
    to.setCoordinateX(from.getCoordinateX());
    to.setCoordinateY(from.getCoordinateY());
    to.setGridOfficeId(from.getId());
    return to;
  }

  private static Wall convertCornersToWall(
      WallDto from,
      Corner fromFirstCorner,
      Corner fromSecondCorner) {

    Wall to = new Wall();

    if (from == null) {
      return to;
    }

    to.setCorner1(fromFirstCorner);
    to.setCorner2(fromSecondCorner);

    return to;
  }

  private static Corner convertCornerDtoToCorner(
      CornerDto from) {

    Corner to = new Corner();

    if (from == null) {
      return to;
    }

    to.setFrontendId(from.getId());
    to.setCoordinateX(from.getCoordinateX());
    to.setCoordinateY(from.getCoordinateY());

    return to;
  }

}
