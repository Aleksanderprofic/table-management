package com.gd.tablesmanagement.managers.floorplan;

import static com.gd.tablesmanagement.converters.floorplan.FloorPlanRequestDtoToFloorConverter.convertItemDtoListToDeskList;
import static com.gd.tablesmanagement.converters.floorplan.FloorPlanRequestDtoToFloorConverter.convertItemDtoListToRoomDescriptionList;
import static com.gd.tablesmanagement.converters.floorplan.FloorPlanRequestDtoToFloorConverter.convertWallDtoListAndCornerDtoListToWallList;

import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanRequestDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanResponseDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.ItemDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorInfoDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemType;
import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemTypeDto;
import com.gd.tablesmanagement.converters.floorplan.FloorPlanRequestDtoToFloorConverter;
import com.gd.tablesmanagement.converters.floorplan.FloorToFloorInfoDtoConverter;
import com.gd.tablesmanagement.converters.floorplan.FloorToFloorPlanResponseDtoConverter;
import com.gd.tablesmanagement.events.FloorUpdatedEvent;
import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.RoomDescription;
import com.gd.tablesmanagement.persistance.model.Wall;
import com.gd.tablesmanagement.persistance.repositories.FloorRepository;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.gd.tablesmanagement.services.google.sheets.reader.GoogleSheetsReader;
import com.gd.tablesmanagement.services.persistance.TransactionalDbOperations;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class  FloorPlanManager {

  private final GoogleSheetsReader googleSheetsReader;
  private final TransactionalDbOperations transactionalDbOperations;

  private final FloorRepository floorRepository;

  private final SheetIdentity sheet;
  private final ApplicationEventPublisher searchEventsPublisher;


  private static final Logger logger = LoggerFactory.getLogger(FloorPlanManager.class);

  /** Manager for CRUD for floor plan.
   * @param googleSheetsReader googleSheetsReader
   * @param floorRepository floorRepository
   * @param spreadsheetId spreadsheetId
   * @param sheetName sheetName
   * @param publisher publisher
   * @param transactionalDbOperations transactionalDbOperations
   */
  public FloorPlanManager(
      GoogleSheetsReader googleSheetsReader,
      FloorRepository floorRepository,
      @Value("${google.sheets.spreadsheet-id}") String spreadsheetId,
      @Value("${google.sheets.sheet-name}") String sheetName,
      ApplicationEventPublisher publisher,
      TransactionalDbOperations transactionalDbOperations) {
    this.googleSheetsReader = googleSheetsReader;
    this.floorRepository = floorRepository;
    this.searchEventsPublisher = publisher;
    this.transactionalDbOperations = transactionalDbOperations;

    sheet = new SheetIdentity(spreadsheetId, sheetName);
  }

  /** Gets from database list of all floors with their ids and names.
   * @return list of all floors with their ids and names.
   */
  public FloorInfoDto[] getFloorPlanList() {

    List<FloorInfoDto> floorInfoDtos = new LinkedList<>();

    floorRepository
        .findAll()
        .forEach(floor -> floorInfoDtos.add(FloorToFloorInfoDtoConverter.convert(floor)));

    return floorInfoDtos.toArray(new FloorInfoDto[]{});

  }

  /** Creates new Floor Plan in database.
   * @param createFloorPlanRequestDto  request to parse
   * @return parsed response containing saved data and id
   */
  public FloorPlanResponseDto createFloorPlan(
      FloorPlanRequestDto createFloorPlanRequestDto) {

    String floorName = createFloorPlanRequestDto.getFloorMetaData().getName();
    if (floorRepository.existsByName(floorName)) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          "Floor with name " + floorName + " already exists");
    }

    Floor savedFloor = floorRepository
        .save(FloorPlanRequestDtoToFloorConverter.convert(createFloorPlanRequestDto));

    searchEventsPublisher.publishEvent(new FloorUpdatedEvent(savedFloor.getId()));

    return FloorToFloorPlanResponseDtoConverter.convert(savedFloor);
  }

  /**  Loads floor data from database and if needed updated it from google spreadsheet.
   * @param floorId  floor id
   * @return parsed response containing floor data and id.
   */
  public FloorPlanResponseDto loadFloorPlan(Long floorId) {

    Floor floor = floorRepository
        .findFloorById(floorId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Floor not found"));

    return FloorToFloorPlanResponseDtoConverter.convert(floor);
  }

  /** Gets list of all possible item types from corresponding enum.
   *
   * @return list of all possible item types (id + label)
   */
  public ItemTypeDto[] getItemTypeList() {

    return Arrays.stream(ItemType.values())
        .map(
            itemType -> {
              ItemTypeDto itemTypeDto = new ItemTypeDto();
              itemTypeDto.setId(itemType.getTypeId());
              itemTypeDto.setLabel(itemType.getLabel());
              return itemTypeDto;
            })
        .collect(Collectors.toList())
        .toArray(new ItemTypeDto[]{});
  }

  /**  Updates floor in db with given id from given request, if needed uses spreadsheets to
   *   load/update data, can't delete desks loaded from spreadsheets, for items performs
   *   specified action (NONE if null), for walls/corners overrides them.
   *
   * @param floorId  id of floor to update
   * @param updateFloorPlanRequest  request with floor data
   * @return response containing updated floor
   */
  public FloorPlanResponseDto updateFloorPlan(
      Long floorId,
      FloorPlanRequestDto updateFloorPlanRequest) {

    updateFloorData(floorId, updateFloorPlanRequest);

    searchEventsPublisher.publishEvent(new FloorUpdatedEvent(floorId));

    return loadFloorPlan(floorId);
  }

  private void updateFloorData(
      Long floorId,
      FloorPlanRequestDto updateFloorPlanRequest) {

    Floor floorData =  new Floor();

    String floorName = updateFloorPlanRequest.getFloorMetaData().getName();
    floorData.setName(floorName);

    List<ItemDto> requestItems = updateFloorPlanRequest.getFloorPlanDto().getItems();
    List<Desk> deskData = convertItemDtoListToDeskList(requestItems);
    floorData.setDesks(deskData);

    List<Wall> wallData = convertWallDtoListAndCornerDtoListToWallList(
        updateFloorPlanRequest.getFloorPlanDto().getCorners(),
        updateFloorPlanRequest.getFloorPlanDto().getWalls()
    );
    floorData.setWalls(wallData);

    List<RoomDescription> descriptionData = convertItemDtoListToRoomDescriptionList(requestItems);
    floorData.setRoomDescriptions(descriptionData);

    try {
      transactionalDbOperations.updateFloorById(floorId, floorData);
    } catch (IllegalArgumentException ex) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Floor not found");
    }

  }

  private void updateDeskDataInSpreadsheet(
      List<Desk> desksToOverride) {

    for (Desk desk : desksToOverride) {

      try {
        googleSheetsReader.saveDesk(sheet, desk);
      } catch (Exception ex) {
        logger.debug(ex.getMessage(), ex);
      }

    }
  }

  public void deleteFloorPlan(Long id) {
    floorRepository.deleteById(id);
  }

  private Floor mergeFloorsDesksWithSpreadsheetDesks(
      Floor floor,
      List<Desk> spreadsheetDesks) {

    Map<Long, Desk> spreadsheetDesksById = spreadsheetDesks
        .stream()
        .collect(Collectors.toMap(Desk::getGridOfficeId, d -> d));

    List<Desk> mergedDesks = new LinkedList<>();

    floor.getDesks().forEach(dbDesk -> mergedDesks.add(mergeDbDeskWithSpreadsheetDesk(
        dbDesk,
        spreadsheetDesksById.get(dbDesk.getGridOfficeId()))));

    floor.setDesks(mergedDesks);
    return floor;
  }

  private Desk mergeDbDeskWithSpreadsheetDesk(Desk dbDesk, @Nullable Desk spreadSheetDesk) {

    if (spreadSheetDesk == null) {
      return dbDesk;
    }

    dbDesk.setEmployee(spreadSheetDesk.getEmployee());

    return dbDesk;
  }


}
