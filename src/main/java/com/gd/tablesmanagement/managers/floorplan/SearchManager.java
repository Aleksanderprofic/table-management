package com.gd.tablesmanagement.managers.floorplan;

import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanResponseDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.ItemDto;
import com.gd.tablesmanagement.events.FloorUpdatedEvent;
import com.gd.tablesmanagement.services.search.desks.TypeAheadSearchService;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SearchManager {

  private static final Logger logger = LoggerFactory.getLogger(SearchManager.class);

  private final FloorPlanManager floorPlanManager;
  private Map<Long, TypeAheadSearchService<ItemDto>> searchByNameServices = new HashMap<>();

  /** Service for managing search.
   *
   * @param floorPlanManager  manager to load data from when search is invalidated.
   */
  public SearchManager(
      FloorPlanManager floorPlanManager) {
    this.floorPlanManager = floorPlanManager;

  }

  /** Returns list of desks matching given query.
   *
   * @param nameQuery  query by which desks will be searched
   * @param account  if not null only desks will be filtered by this account
   * @return all desks that match given query sorted alphabetically
   */
  public LinkedHashSet<ItemDto> searchItemsByNameAndAccount(
      Long floorId,
      String nameQuery,
      String account)
      throws ExecutionException, InterruptedException {

    logger.debug("Searching for desks matching name \"{}\" {}", nameQuery, account);

    TypeAheadSearchService<ItemDto> floorSearchService =  searchByNameServices.get(floorId);

    if (floorSearchService == null) {
      floorSearchService = handleSearchDataInvalidation(new FloorUpdatedEvent(floorId)).get();
    }

    LinkedHashSet<ItemDto> searchByNameResults =
        floorSearchService.getSearchResult(nameQuery).get();

    if (account != null) {
      return searchByNameResults
          .stream()
          .filter(itemDto -> toUpperAndTrim(account)
              .equals(toUpperAndTrim(itemDto.getAccount())))
          .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    return searchByNameResults;
  }

  /** Handler for search data invalidation.
   *
   * @param event  event for invalidating search data
   */
  @Async
  @EventListener
  public Future<TypeAheadSearchService<ItemDto>> handleSearchDataInvalidation(
      FloorUpdatedEvent event) {

    Long floorId = event.getFloorId();

    FloorPlanResponseDto floorResponse = floorPlanManager.loadFloorPlan(floorId);

    logger.debug("Recreating searching data");

    List<ItemDto> floorItems = floorResponse.getFloorPlanDto().getItems();

    TypeAheadSearchService<ItemDto> searchService = new TypeAheadSearchService<>(ItemDto::getName);
    searchService.updateSearchingData(floorItems);

    searchByNameServices.put(floorId, searchService);

    return CompletableFuture.completedFuture(searchService);
  }

  private String toUpperAndTrim(String string) {

    if (string != null) {
      return string.toUpperCase().trim();
    }

    return "";
  }
}
