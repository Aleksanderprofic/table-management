package com.gd.tablesmanagement.config.google.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SheetsConfig {

  private final Sheets sheets;

  /**
   * Configuration for google's sheets service.
   *
   * @param applicationName name of application used when calling google API.
   * @param credentials service account credentials JSON for google application
   * @throws IOException when thrown during reading credentials
   * @throws GeneralSecurityException when thrown by google API when checking credentials
   */
  public SheetsConfig(@Value("${google.sheets.application-name}") String applicationName,
      @Value("${google.sheets.credentials}") String credentials)
      throws IOException, GeneralSecurityException {

    InputStream is = new ByteArrayInputStream(credentials.getBytes());

    Credential credential = GoogleCredential
        .fromStream(is)
        .createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS));

    sheets = new Sheets.Builder(
        GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(),
        credential)
        .setApplicationName(applicationName)
        .build();
  }

  @Bean
  public Sheets getSheets() {
    return sheets;
  }

}
