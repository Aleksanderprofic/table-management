package com.gd.tablesmanagement.config.security;

import com.gd.tablesmanagement.filters.security.BearerAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.session.ConcurrentSessionFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final BearerAuthenticationFilter bearerAuthenticationFilter;

  public SecurityConfig(
      BearerAuthenticationFilter bearerAuthenticationFilter) {
    this.bearerAuthenticationFilter = bearerAuthenticationFilter;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.addFilterAfter(bearerAuthenticationFilter, ConcurrentSessionFilter.class);

    http
        .cors()
        .and()
        .csrf()
        .disable()
        .authorizeRequests()
        .anyRequest()
        .authenticated();
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring().mvcMatchers(
        "/swagger-ui.html/**",
        "/configuration/**",
        "/swagger-resources/**",
        "/v2/api-docs",
        "/webjars/**");
  }

  /**
   * Cors configuration bean.
   */
  @Bean
  public WebMvcConfigurer configurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry
            .addMapping("/**")
            .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS", "PATCH")
            .allowedOrigins("*")
            .allowedHeaders("Authorization", "Cache-Control", "Content-Type", "Accept",
                "X-Requested-With", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers",
                "Origin")
            .exposedHeaders("Access-Control-Expose-Headers", "Authorization", "Cache-Control",
                "Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers",
                "Origin")
            .allowCredentials(true);
      }
    };
  }
}