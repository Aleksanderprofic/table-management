package com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;

public class FloorInfoDto {

  @JsonProperty("id")
  private Long id;

  @JsonProperty("data")
  @Valid
  private FloorMetaData data;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public FloorMetaData getData() {
    return data;
  }

  public void setData(FloorMetaData data) {
    this.data = data;
  }

}
