package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public class FloorPlanResponseDto extends FloorPlanRequestDto {

  @JsonProperty("id")
  @NotNull(message = "floor id is a mandatory argument")
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FloorPlanResponseDto)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    FloorPlanResponseDto that = (FloorPlanResponseDto) o;
    return Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), getId());
  }
}
