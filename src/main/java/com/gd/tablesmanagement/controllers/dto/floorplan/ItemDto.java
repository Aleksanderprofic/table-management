package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public class ItemDto {

  @JsonProperty("type")
  @NotNull(message = "items's type is mandatory argument")
  private Long type;

  @JsonProperty("description")
  @NotNull(message = "items's description is mandatory argument")
  private String description;

  @JsonProperty("id")
  @NotNull(message = "items's id is mandatory argument")
  private Long id;

  @JsonProperty("name")
  @NotNull(message = "items's name is mandatory argument")
  private String name;

  @JsonProperty("r")
  @NotNull(message = "items's r is mandatory argument")
  private Double rotation;

  @JsonProperty("x")
  @NotNull(message = "items's x is mandatory argument")
  private Double coordinateX;

  @JsonProperty("y")
  @NotNull(message = "items's y is mandatory argument")
  private Double coordinateY;

  @JsonProperty("account")
  private String account;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getRotation() {
    return rotation;
  }

  public void setRotation(Double rotation) {
    this.rotation = rotation;
  }

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }

  public Long getType() {
    return type;
  }

  public void setType(Long type) {
    this.type = type;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ItemDto)) {
      return false;
    }
    ItemDto itemDto = (ItemDto) o;
    return Objects.equals(getType(), itemDto.getType())
        && Objects.equals(getDescription(), itemDto.getDescription())
        && Objects.equals(getId(), itemDto.getId())
        && Objects.equals(getName(), itemDto.getName())
        && Objects.equals(getRotation(), itemDto.getRotation())
        && Objects.equals(getCoordinateX(), itemDto.getCoordinateX())
        && Objects.equals(getCoordinateY(), itemDto.getCoordinateY())
        && Objects.equals(getAccount(), itemDto.getAccount());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(
            getType(),
            getDescription(),
            getId(),
            getName(),
            getRotation(),
            getCoordinateX(),
            getCoordinateY(),
            getAccount()
        );
  }
}
