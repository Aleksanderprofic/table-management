package com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FloorMetaData {

  @JsonProperty("name")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
