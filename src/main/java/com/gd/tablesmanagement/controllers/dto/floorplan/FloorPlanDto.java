package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class FloorPlanDto {

  @JsonProperty("walls")
  @NotNull(message = "plan's walls is mandatory argument")
  @Valid
  private List<WallDto> walls;

  @JsonProperty("corners")
  @NotNull(message = "plan's corners is mandatory argument")
  @Valid
  private List<CornerDto> corners;

  @JsonProperty("items")
  @NotNull(message = "plan's items is mandatory argument")
  @Valid
  private List<ItemDto> items;

  public List<WallDto> getWalls() {
    return walls;
  }

  public void setWalls(List<WallDto> walls) {
    this.walls = walls;
  }

  public List<CornerDto> getCorners() {
    return corners;
  }

  public void setCorners(
      List<CornerDto> corners) {
    this.corners = corners;
  }

  public List<ItemDto> getItems() {
    return items;
  }

  public void setItems(List<ItemDto> items) {
    this.items = items;
  }

  /** Adds all items from list into floor plan.
   *
   * @param  items items to add
   */
  public void addItems(List<ItemDto> items) {
    if (this.items == null) {
      this.items = new LinkedList<>();
    }
    this.items.addAll(items);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FloorPlanDto)) {
      return false;
    }
    FloorPlanDto that = (FloorPlanDto) o;
    return Objects.equals(getWalls(), that.getWalls())
        && Objects.equals(getCorners(), that.getCorners())
        && Objects.equals(getItems(), that.getItems());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getWalls(), getCorners(), getItems());
  }
}
