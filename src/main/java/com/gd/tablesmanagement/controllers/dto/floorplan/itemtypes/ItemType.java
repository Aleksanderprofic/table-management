package com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes;

public enum ItemType {

  DESK(0L, "Desk"),
  ROOM_DESCRIPTION(1L, "Room Description");

  private Long typeId;
  private String label;

  ItemType(Long typeId, String label) {
    this.typeId = typeId;
    this.label = label;
  }

  public Long getTypeId() {
    return typeId;
  }

  public String getLabel() {
    return label;
  }
}
