package com.gd.tablesmanagement.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;

public class UpdateDeskDto {

  @JsonProperty("employee_name")
  @NotNull(message = "employee_name is mandatory argument")
  private String employeeName;

  @JsonProperty("employee_account")
  @NotNull(message = "employee_account is mandatory argument")
  private String account;

  @JsonProperty("desk_internal_id")
  private Long deskInternalId;

  @JsonProperty("x_coordinate")
  @NotNull(message = "x_coordinate is mandatory argument")
  private Double coordinateX;

  @JsonProperty("y_coordinate")
  @NotNull(message = "y_coordinate is mandatory argument")
  private Double coordinateY;

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public Long getDeskInternalId() {
    return deskInternalId;
  }

  public void setDeskInternalId(Long tableInternalId) {
    this.deskInternalId = tableInternalId;
  }

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }
}
