package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public class CornerDto {

  @JsonProperty("id")
  @NotNull(message = "corner's id is mandatory argument")
  private String id;

  @JsonProperty("x")
  @NotNull(message = "corner's x is mandatory argument")
  private Double coordinateX;

  @JsonProperty("y")
  @NotNull(message = "corner's y is mandatory argument")
  private Double coordinateY;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CornerDto)) {
      return false;
    }
    CornerDto cornerDto = (CornerDto) o;
    return Objects.equals(getId(), cornerDto.getId())
        && Objects.equals(getCoordinateX(), cornerDto.getCoordinateX())
        && Objects.equals(getCoordinateY(), cornerDto.getCoordinateY());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getCoordinateX(), getCoordinateY());
  }
}
