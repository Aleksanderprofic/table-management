package com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemTypeDto {

  @JsonProperty("id")
  private Long id;

  @JsonProperty("label")
  private String label;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
