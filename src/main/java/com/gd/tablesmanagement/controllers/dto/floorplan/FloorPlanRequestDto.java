package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorMetaData;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class FloorPlanRequestDto {

  @JsonProperty("data")
  @NotNull(message = "data is mandatory argument")
  @Valid
  private FloorMetaData floorMetaData;

  @JsonProperty("plan")
  @NotNull(message = "plan is mandatory argument")
  @Valid
  private FloorPlanDto floorPlanDto;

  public void setFloorMetaData(
      FloorMetaData floorMetaData) {
    this.floorMetaData = floorMetaData;
  }

  public FloorMetaData getFloorMetaData() {
    return floorMetaData;
  }

  public FloorPlanDto getFloorPlanDto() {
    return floorPlanDto;
  }

  public void setFloorPlanDto(FloorPlanDto floorPlanDto) {
    this.floorPlanDto = floorPlanDto;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FloorPlanRequestDto)) {
      return false;
    }
    FloorPlanRequestDto that = (FloorPlanRequestDto) o;
    return Objects.equals(getFloorMetaData(), that.getFloorMetaData())
        && Objects.equals(getFloorPlanDto(), that.getFloorPlanDto());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getFloorMetaData(), getFloorPlanDto());
  }
}
