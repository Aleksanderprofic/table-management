package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class DeskDto {

  @JsonProperty("description")
  private String description;

  @JsonProperty("id")
  private Long id;

  @JsonProperty("employee_name")
  private String employeeName;

  @JsonProperty("r")
  private Double rotation;

  @JsonProperty("x")
  private Double coordinateX;

  @JsonProperty("y")
  private Double coordinateY;

  @JsonProperty("account")
  private String accountName;

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }

  public Double getRotation() {
    return rotation;
  }

  public void setRotation(Double rotation) {
    this.rotation = rotation;
  }

  public Double getCoordinateX() {
    return coordinateX;
  }

  public void setCoordinateX(Double coordinateX) {
    this.coordinateX = coordinateX;
  }

  public Double getCoordinateY() {
    return coordinateY;
  }

  public void setCoordinateY(Double coordinateY) {
    this.coordinateY = coordinateY;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DeskDto)) {
      return false;
    }
    DeskDto deskDto = (DeskDto) o;
    return Objects.equals(getDescription(), deskDto.getDescription())
        && Objects.equals(getId(), deskDto.getId())
        && Objects.equals(getEmployeeName(), deskDto.getEmployeeName())
        && Objects.equals(getRotation(), deskDto.getRotation())
        && Objects.equals(getCoordinateX(), deskDto.getCoordinateX())
        && Objects.equals(getCoordinateY(), deskDto.getCoordinateY())
        && Objects.equals(getAccountName(), deskDto.getAccountName());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(
            getDescription(),
            getId(),
            getEmployeeName(),
            getRotation(),
            getCoordinateX(),
            getCoordinateY(),
            getAccountName());
  }
}
