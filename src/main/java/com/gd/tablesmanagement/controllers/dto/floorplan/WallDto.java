package com.gd.tablesmanagement.controllers.dto.floorplan;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public class WallDto {

  @JsonProperty("corner1")
  @NotNull(message = "wall's corner1 is mandatory argument")
  private String corner1;

  @JsonProperty("corner2")
  @NotNull(message = "wall's corner2 is mandatory argument")
  private String corner2;

  public String getCorner1() {
    return corner1;
  }

  public void setCorner1(String corner1) {
    this.corner1 = corner1;
  }

  public String getCorner2() {
    return corner2;
  }

  public void setCorner2(String corner2) {
    this.corner2 = corner2;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof WallDto)) {
      return false;
    }
    WallDto wallDto = (WallDto) o;
    return Objects.equals(getCorner1(), wallDto.getCorner1())
        && Objects.equals(getCorner2(), wallDto.getCorner2());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCorner1(), getCorner2());
  }
}
