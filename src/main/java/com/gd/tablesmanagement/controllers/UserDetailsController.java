package com.gd.tablesmanagement.controllers;

import com.gd.tablesmanagement.controllers.dto.LoggedInUserDetailsDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Api(value = "User details", description = "Endpoint for getting user details")
public class UserDetailsController {

  /**
   * Endpoint for returning logged in user details.
   * @param authentication Authentication object
   * @return Logged in user details
   */
  @GetMapping("/userDetails")
  @ApiOperation(
      value = "Returns details of logged in user",
      response = LoggedInUserDetailsDto.class)
  public ResponseEntity<LoggedInUserDetailsDto> getUserDetails(Authentication authentication) {

    Map<String, Object> attributes = ((DefaultOidcUser) authentication.getPrincipal())
        .getAttributes();

    String userName = attributes.get("name").toString();
    String userPicture = attributes.get("picture").toString();

    LoggedInUserDetailsDto loggedInUser = new LoggedInUserDetailsDto(userName, userPicture);

    return ResponseEntity.ok(loggedInUser);
  }
}
