package com.gd.tablesmanagement.controllers.floorplan;

import static org.springframework.http.ResponseEntity.ok;

import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanRequestDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.FloorPlanResponseDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.ItemDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.floorinfo.FloorInfoDto;
import com.gd.tablesmanagement.controllers.dto.floorplan.itemtypes.ItemTypeDto;
import com.gd.tablesmanagement.managers.floorplan.FloorPlanManager;
import com.gd.tablesmanagement.managers.floorplan.SearchManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/floors")
@Api(value = "Floor Plan Controller")
public class FloorPlanController {

  private final FloorPlanManager floorPlanManager;
  private final SearchManager searchManager;

  public FloorPlanController(
      FloorPlanManager floorPlanManager,
      SearchManager searchManager) {
    this.floorPlanManager = floorPlanManager;
    this.searchManager = searchManager;
  }

  @GetMapping(value = "/itemTypeList")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<ItemTypeDto[]> getItemTypeList() {
    return ok().body(floorPlanManager.getItemTypeList());
  }

  @GetMapping(value = "/floorPlanList")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<FloorInfoDto[]> getFloorPlanList() {
    return ok().body(floorPlanManager.getFloorPlanList());
  }

  @GetMapping(value = "/floorPlan/{id}")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<FloorPlanResponseDto> getFloorPlan(
      @PathVariable Long id) {
    return ok().body(floorPlanManager.loadFloorPlan(id));
  }

  @PostMapping(value = "/floorPlan")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<FloorPlanResponseDto> createFloorPlan(
      @Valid @RequestBody FloorPlanRequestDto createFloorPlanRequestDto) {
    return ok().body(floorPlanManager.createFloorPlan(createFloorPlanRequestDto));
  }

  @PutMapping(value = "/floorPlan/{id}")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<FloorPlanResponseDto> updateFloorPlan(
      @PathVariable Long id,
      @Valid @RequestBody FloorPlanRequestDto updateFloorPlanRequest)
      throws IOException {
    return ok().body(floorPlanManager.updateFloorPlan(id, updateFloorPlanRequest));
  }

  @DeleteMapping(value = "/floorPlan/{id}")
  @ApiOperation(
      value = "..."
  )
  public ResponseEntity<Void> deleteFloorPlan(
      @PathVariable Long id) {
    floorPlanManager.deleteFloorPlan(id);
    return ok().build();
  }

  @GetMapping("/floorPlan/{id}/items/search/nameAndAccount")
  @ApiOperation(
      value = "Search",
      response = ResponseEntity.class)
  public ResponseEntity<Set<ItemDto>> searchForDesk(
      @PathVariable Long id,
      @RequestParam(value = "name_query", defaultValue = "") String nameQuery,
      @RequestParam(required = false) String account)
      throws ExecutionException, InterruptedException {
    return ok().body(searchManager.searchItemsByNameAndAccount(id, nameQuery, account));
  }
}
