package com.gd.tablesmanagement.controllers;

import static org.springframework.http.ResponseEntity.badRequest;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppDefaultExceptionHandler {

  /**
   * Exception handler for validation exceptions (@Valid) returning only list of default messages.
   *
   * @param ex Exception to handle
   * @return String list containing default exception messages wrapped in response entity.
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<List<String>> handleValidationExceptions(
      MethodArgumentNotValidException ex) {

    List<String> errors = ex
        .getBindingResult()
        .getAllErrors()
        .stream()
        .map(ObjectError::getDefaultMessage)
        .collect(Collectors.toList());

    return badRequest().body(errors);
  }

  /**
   * Exception handler for errors when accessing google sheets like bad sheet name.
   *
   * @param ex Exception to handle
   * @return exception JSON from Google
   */
  @ExceptionHandler(GoogleJsonResponseException.class)
  public ResponseEntity<GoogleJsonError> handleGoogleSpreadsheetsException(
      GoogleJsonResponseException ex) {
    return badRequest().body(ex.getDetails());
  }

}
