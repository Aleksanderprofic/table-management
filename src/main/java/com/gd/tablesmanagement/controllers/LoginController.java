package com.gd.tablesmanagement.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class LoginController {

  private static final String googleURL = "/oauth2/authorize-client/google";
  private static final String swaggerURL = "/swagger-ui.html";

  /**
   * Returns view of google OAuth logging page.
   *
   * @param model model
   * @return OAuth Login view
   */
  @GetMapping("/oauth_login")
  public String getLoginPage(Model model) {

    return "redirect:" + googleURL;
  }

  /**
   * Checks if user is allowed to login, saves user data into model and returns view of successful
   * OAuth2 login.
   *
   * @param model model
   * @param authentication OAuth2 token
   * @return view of successful OAuth2 login
   */
  @GetMapping("/")
  @PreAuthorize(
      "@securityService"
          + ".isInDatabase(#authentication"
          + ".getPrincipal()"
          + ".getAttributes()"
          + ".get('email'))")
  public String getLoginSuccessPageWithInfo(Model model, OAuth2AuthenticationToken authentication) {
    model.addAttribute("name", authentication.getPrincipal().getAttributes().get("name"));
    model.addAttribute("email", authentication.getPrincipal().getAttributes().get("email"));

    return "redirect:" + swaggerURL;
  }
}