package com.gd.tablesmanagement.services.google.sheets.data;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.Test;

public class CellPositionTest {

  private final CellPosition a1 = new CellPosition("A1");
  private final CellPosition c1 = new CellPosition("C1");
  private final CellPosition a5 = new CellPosition("A5");
  private final CellPosition aa12 = new CellPosition("AA12");

  @Test
  public void getRowTest() {
    assertAll(
        () -> assertEquals(1, a1.getRow()),
        () -> assertEquals(1, c1.getRow()),
        () -> assertEquals(5, a5.getRow()),
        () -> assertEquals(12, aa12.getRow())
    );
  }

  @Test
  public void getColumnTest() {
    assertAll(
        () -> assertEquals("A", a1.getColumn()),
        () -> assertEquals("C", c1.getColumn()),
        () -> assertEquals("A", a5.getColumn()),
        () -> assertEquals("AA", aa12.getColumn())
    );
  }

  @Test
  public void getAsStringTest() {
    assertAll(
        () -> assertEquals("A1", a1.getAsString()),
        () -> assertEquals("C1", c1.getAsString()),
        () -> assertEquals("A5", a5.getAsString()),
        () -> assertEquals("AA12", aa12.getAsString())
    );
  }

  @Test
  public void plusRows() {
    assertAll(
        () -> assertEquals(a5, a1.plusRows(4)),
        () -> assertEquals(new CellPosition("A31"), a5.plusRows(26))
    );
  }

  @Test
  public void plusColumns() {
    assertAll(
        () -> assertEquals(c1, a1.plusColumns(2)),
        () -> assertEquals(new CellPosition("AA5"), a5.plusColumns(26))
    );
  }
}