package com.gd.tablesmanagement.services.google.sheets.data;

import static org.junit.Assert.assertEquals;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import org.junit.Test;

public class ColumnTypeTest {

  @Test
  public void updateDeskFromCellUpdatesIdFromStringTest() {

    Desk desk = new Desk();
    Object data = "1";
    Long id = 1L;

    ColumnType.DESK_ID.updateDeskFromCell(desk, data);
    assertEquals(id, desk.getGridOfficeId());

  }

  @Test
  public void updateDeskFromCellUpdatesIdFromIntegerTest() {

    Desk desk = new Desk();
    Long id = 1L;

    ColumnType.DESK_ID.updateDeskFromCell(desk, id);
    assertEquals(id, desk.getGridOfficeId());

  }

  @Test
  public void updateDeskFromCellUpdatesEmployeeNameFromStringTest() {

    Desk desk = new Desk();
    String name = "name";

    ColumnType.EMPLOYEE_NAME.updateDeskFromCell(desk, name);
    assertEquals(name, desk.getEmployee().getName());

  }

  @Test
  public void updateDeskFromCellUpdatesEmployeeAccountFromStringTest() {

    Desk desk = new Desk();
    String account = "account";

    ColumnType.EMPLOYEE_ACCOUNT.updateDeskFromCell(desk, account);
    assertEquals(account, desk.getEmployee().getAccount());

  }

  @Test
  public void getCellValueFromDeskGetsDeskIdTest() {

    Desk desk = new Desk();
    long id = 1;
    desk.setGridOfficeId(id);

    assertEquals(id, ColumnType.DESK_ID.getCellValueFromDesk(desk));

  }

  @Test
  public void getCellValueFromDeskGetsEmployeeNameTest() {

    Desk desk = new Desk();
    Employee employee = new Employee();
    String name = "name";
    employee.setName(name);
    desk.setEmployee(employee);

    assertEquals(name, ColumnType.EMPLOYEE_NAME.getCellValueFromDesk(desk));

  }

  @Test
  public void getCellValueFromDeskGetsEmployeeAccountTest() {

    Desk desk = new Desk();
    Employee employee = new Employee();
    String account = "account";
    employee.setAccount(account);
    desk.setEmployee(employee);

    assertEquals(account, ColumnType.EMPLOYEE_ACCOUNT.getCellValueFromDesk(desk));

  }
}