package com.gd.tablesmanagement.services.google.sheets.processors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Test;

public class SheetProcessorTest {

  private final SheetIdentity sheet = new SheetIdentity("id", "name");

  private final SheetsService sheetsService = mock(SheetsService.class);
  private final GridRange gridRange = new GridRange(
      new CellPosition("A1"),
      new CellPosition("C3"));
  private final List<ColumnType> columns = Arrays.asList(
      ColumnType.DESK_ID,
      ColumnType.EMPLOYEE_NAME,
      ColumnType.EMPLOYEE_ACCOUNT);

  private final SheetProcessor sheetProcessor = new SheetProcessor(sheetsService, gridRange,
      columns);
  private final GridValues gridValues = mock(GridValues.class);
  private final List<Desk> desks = Collections.singletonList(mock(Desk.class));


  @Test
  public void findDesksGetsDesksFromSheetServiceTest() throws IOException {

    when(sheetsService.loadSpreadSheet(sheet, gridRange)).thenReturn(gridValues);
    when(gridValues.parseToDesksByColumns(columns)).thenReturn(desks);

    assertEquals(desks, sheetProcessor.findDesks(sheet));

  }

  @Test
  public void findCellPositionForTableIdFindsIdPositionTest() throws IOException {

    long deskId = 1;
    List<List<Object>> retrievedGridObjects = Arrays.asList(
        Arrays.asList("definitely not id", "employee1", "account"),
        Arrays.asList("0", "employee2", "account"),
        Arrays.asList("1", "employee3", "account")
    );
    CellPosition idPosition = gridRange.getStart().plusRows(2);

    when(sheetsService.loadSpreadSheet(sheet, gridRange)).thenReturn(gridValues);
    when(gridValues.getValues()).thenReturn(retrievedGridObjects);

    assertEquals(Optional.of(idPosition), sheetProcessor.findCellPositionForTableId(sheet, deskId));
  }

  @Test
  public void findCellPositionForTableIdReturnsEmptyWhenIdNotFoundTest() throws IOException {

    long deskId = 3;
    List<List<Object>> retrievedGridObjects = Arrays.asList(
        Arrays.asList("definitely not id", "employee1", "account"),
        Arrays.asList("0", "employee2", "account"),
        Arrays.asList("1", "employee3", "account")
    );

    when(sheetsService.loadSpreadSheet(sheet, gridRange)).thenReturn(gridValues);
    when(gridValues.getValues()).thenReturn(retrievedGridObjects);

    assertEquals(Optional.empty(), sheetProcessor.findCellPositionForTableId(sheet, deskId));
  }

}