package com.gd.tablesmanagement.services.google.sheets.data;

import static org.junit.Assert.assertEquals;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Test;

public class GridValuesTest {

  private List<List<Object>> gridToParse = Arrays.asList(
      Arrays.asList("1", "name", "account"),
      Arrays.asList("2", null, "account2"),
      Arrays.asList("3", "name2"),
      Collections.singletonList("4")
  );

  private List<ColumnType> columns = Arrays.asList(
      ColumnType.DESK_ID,
      ColumnType.EMPLOYEE_NAME,
      ColumnType.EMPLOYEE_ACCOUNT);

  private GridValues gridValues = new GridValues(gridToParse);

  @Test
  public void fromEmployeeTableAndBackTest() {

    Employee employee1 = new Employee();
    employee1.setName("name");
    employee1.setAccount("account");

    Desk desk1 = new Desk();
    desk1.setEmployee(employee1);
    desk1.setGridOfficeId(1L);

    GridValues values = GridValues.fromDesk(desk1, columns);
    assertEquals(desk1, values.parseToDesksByColumns(columns).get(0));

  }

  @Test
  public void parseToEmployeeTablesByColumnsTest() {

    Employee employee1 = new Employee();
    employee1.setName("name");
    employee1.setAccount("account");

    Employee employee2 = new Employee();
    employee2.setAccount("account2");

    Employee employee3 = new Employee();
    employee3.setName("name2");

    Desk desk1 = new Desk();
    desk1.setEmployee(employee1);
    desk1.setGridOfficeId(1L);

    Desk desk2 = new Desk();
    desk2.setEmployee(employee2);
    desk2.setGridOfficeId(2L);

    Desk desk3 = new Desk();
    desk3.setEmployee(employee3);
    desk3.setGridOfficeId(3L);

    Desk desk4 = new Desk();
    desk4.setGridOfficeId(4L);

    List<Desk> parsedTables = Arrays.asList(desk1, desk2, desk3, desk4);

    assertEquals(parsedTables, gridValues.parseToDesksByColumns(columns));

  }
}