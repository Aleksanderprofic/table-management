package com.gd.tablesmanagement.services.google.sheets.data;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.Test;

public class GridRangeTest {

  @Test
  public void getRangeStringConsistentWithGoogleApiTest() {

    GridRange range = new GridRange(new CellPosition("A1"), new CellPosition("A10"));
    GridRange range2 = new GridRange(new CellPosition("CC1"), new CellPosition("DD999"));

    SheetIdentity empty = new SheetIdentity("", "");
    SheetIdentity sheet = new SheetIdentity("id", "name");
    SheetIdentity sheet2 = new SheetIdentity("idd", "long name");

    assertAll(
        () -> assertEquals("A1:A10", range.getRangeString(empty)),
        () -> assertEquals("name!A1:A10", range.getRangeString(sheet)),
        () -> assertEquals("long name!A1:A10", range.getRangeString(sheet2)),
        () -> assertEquals("name!CC1:DD999", range2.getRangeString(sheet))
    );
  }
}