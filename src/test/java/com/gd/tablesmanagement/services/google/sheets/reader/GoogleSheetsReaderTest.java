package com.gd.tablesmanagement.services.google.sheets.reader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.services.google.sheets.SheetsService;
import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.ColumnType;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.gd.tablesmanagement.services.google.sheets.processors.SheetProcessor;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.Test;

public class GoogleSheetsReaderTest {

  private final SheetsService sheetsService = mock(SheetsService.class);

  private final SheetProcessor sheetProcessor = mock(SheetProcessor.class);
  private final SheetProcessor secondSheetProcessor = mock(SheetProcessor.class);

  private final String sheetId = "sheetId";
  private final String sheetName = "sheetName";
  private final SheetIdentity sheet = new SheetIdentity(sheetId, sheetName);

  private final GoogleSheetsReader googleSheetsReader = new GoogleSheetsReader(
      sheetsService,
      Arrays.asList(sheetProcessor, secondSheetProcessor)
  );

  @Test
  public void findAllDesksReturnsAllDesksTest() throws IOException {

    Desk desk = new Desk();
    desk.setGridOfficeId(1L);
    Desk desk2 = new Desk();
    desk2.setGridOfficeId(2L);
    Desk desk3 = new Desk();
    desk3.setGridOfficeId(3L);
    Desk desk4 = new Desk();
    desk4.setGridOfficeId(4L);

    when(sheetProcessor.findDesks(sheet)).thenReturn(Arrays.asList(desk, desk2));
    when(secondSheetProcessor.findDesks(sheet)).thenReturn(Arrays.asList(desk3, desk4));

    assertThat(googleSheetsReader.findAllDesks(sheet))
        .containsAll(Arrays.asList(desk, desk2, desk3, desk4));

  }

  @Test
  public void saveDeskSavesDeskWhenFoundTest() throws IOException {

    long id = 1;

    Employee employee = new Employee();
    employee.setName("name");

    Desk desk = new Desk();
    desk.setGridOfficeId(id);
    desk.setEmployee(employee);

    List<ColumnType> columns = Arrays.asList(ColumnType.DESK_ID, ColumnType.EMPLOYEE_NAME);

    GridValues gridToSave = GridValues.fromDesk(desk, columns);

    CellPosition position = new CellPosition("A1");

    UpdateValuesResponse response = new UpdateValuesResponse();

    when(sheetProcessor.findCellPositionForTableId(sheet, id)).thenReturn(Optional.empty());
    when(secondSheetProcessor.getColumns()).thenReturn(columns);
    when(secondSheetProcessor.findCellPositionForTableId(sheet, id))
        .thenReturn(Optional.of(position));
    when(sheetsService.updateSpreadSheet(eq(sheet), any(), eq(gridToSave))).thenReturn(response);

    assertEquals(Optional.of(response), googleSheetsReader.saveDesk(sheet, desk));

  }

  @Test
  public void saveDeskReturnsEmptyOptionalIfIdNotFoundTest() throws IOException {

    Long id = 1L;

    Employee employee = new Employee();
    employee.setName("name");

    Desk desk = new Desk();
    desk.setGridOfficeId(id);
    desk.setEmployee(employee);

    when(sheetProcessor.findCellPositionForTableId(sheet, id)).thenReturn(Optional.empty());
    when(secondSheetProcessor.findCellPositionForTableId(sheet, id)).thenReturn(Optional.empty());

    assertEquals(Optional.empty(), googleSheetsReader.saveDesk(sheet, desk));

  }
}