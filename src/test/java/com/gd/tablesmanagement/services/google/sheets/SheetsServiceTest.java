package com.gd.tablesmanagement.services.google.sheets;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.gd.tablesmanagement.services.google.sheets.data.CellPosition;
import com.gd.tablesmanagement.services.google.sheets.data.GridRange;
import com.gd.tablesmanagement.services.google.sheets.data.GridValues;
import com.gd.tablesmanagement.services.google.sheets.data.SheetIdentity;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import java.io.IOException;
import org.junit.Test;

public class SheetsServiceTest {

  private final Sheets sheets = mock(Sheets.class);
  private final SheetsService sheetsService = new SheetsService(sheets);

  private final SheetIdentity sheet = new SheetIdentity("id", "name");
  private final GridRange gridRange = new GridRange(
      new CellPosition("A1"),
      new CellPosition("C10"));
  private final ValueRange valueRange = mock(ValueRange.class);
  private final GridValues gridValues = new GridValues(valueRange);

  private final Sheets.Spreadsheets spreadsheets = mock(Sheets.Spreadsheets.class);
  private final Sheets.Spreadsheets.Values spreadsheetsValues = mock(
      Sheets.Spreadsheets.Values.class);

  @Test
  public void loadSpreadSheet() throws IOException {

    String sheetId = sheet.getSpreadsheetId();
    String rangeString = gridRange.getRangeString(sheet);

    Sheets.Spreadsheets.Values.Get spreadsheetsValuesGet = mock(
        Sheets.Spreadsheets.Values.Get.class);

    when(sheets.spreadsheets()).thenReturn(spreadsheets);
    when(spreadsheets.values()).thenReturn(spreadsheetsValues);
    when(spreadsheetsValues.get(sheetId, rangeString)).thenReturn(spreadsheetsValuesGet);
    when(spreadsheetsValuesGet.execute()).thenReturn(valueRange);

    assertEquals(gridValues, sheetsService.loadSpreadSheet(sheet, gridRange));
  }

  @Test
  public void updateSpreadSheet() throws IOException {

    String sheetId = sheet.getSpreadsheetId();
    String rangeString = gridRange.getRangeString(sheet);

    Sheets.Spreadsheets.Values.Update spreadsheetsValuesUpdate = mock(
        Sheets.Spreadsheets.Values.Update.class);
    UpdateValuesResponse response = mock(UpdateValuesResponse.class);

    when(sheets.spreadsheets()).thenReturn(spreadsheets);
    when(spreadsheets.values()).thenReturn(spreadsheetsValues);
    when(spreadsheetsValues.update(sheetId, rangeString, valueRange))
        .thenReturn(spreadsheetsValuesUpdate);
    when(spreadsheetsValuesUpdate.setValueInputOption(any())).thenReturn(spreadsheetsValuesUpdate);
    when(spreadsheetsValuesUpdate.execute()).thenReturn(response);

    assertEquals(response, sheetsService.updateSpreadSheet(sheet, gridRange, gridValues));

  }
}