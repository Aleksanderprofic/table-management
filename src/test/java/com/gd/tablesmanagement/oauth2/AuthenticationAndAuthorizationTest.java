package com.gd.tablesmanagement.oauth2;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.gd.tablesmanagement.persistance.model.Desk;
import com.gd.tablesmanagement.persistance.model.Employee;
import com.gd.tablesmanagement.persistance.model.Floor;
import com.gd.tablesmanagement.persistance.model.Role;
import com.gd.tablesmanagement.persistance.repositories.EmployeeRepository;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableConfigurationProperties
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
public class AuthenticationAndAuthorizationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private EmployeeRepository employeeRepository;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    public void when_authenticatedWithCorrectEmail_expect_accessGranted() throws Exception {

        Role role = new Role();
        Desk employeeTable = new Desk();
        Floor desksFloor = new Floor();
        employeeTable.setFloor(desksFloor);
        employeeTable.setGridOfficeId(0L);

        Employee employee = new Employee("correctName", "correct@griddynamics.com", employeeTable, Collections.singleton(role));

        employeeRepository.save(employee);

        mvc
                .perform(get("/").with(authentication(createToken("correct@griddynamics.com"))))
                .andExpect(status().is(302));
    }

    @Test
    public void when_authenticatedWithIncorrectEmail_expect_accessDenied() throws Exception {
        mvc
                .perform(get("/").with(authentication(createToken("bad email"))))
                .andExpect(status().isForbidden());
    }

    @Test
    public void when_unAuthenticated_expect_redirection() throws Exception {
        mvc.perform(get("/").with(anonymous()))
                .andExpect(status().is4xxClientError());
    }

    private OAuth2AuthenticationToken createToken(String email) {
        Set<GrantedAuthority> authorities = new HashSet<>(AuthorityUtils.createAuthorityList("USER"));
        OAuth2User oAuth2User = new DefaultOAuth2User(authorities, Collections.singletonMap("email", email), "email");
        return new OAuth2AuthenticationToken(oAuth2User, authorities, "login-client");
    }
}